﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Effects : ScriptableObject
{
    public static Sequence CreateWobbleEffect(Transform transform, float duration, float multiplier)
    {
        float scale = transform.localScale.x;
        
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(scale * multiplier, duration / 2));
        seq.Append(transform.DOScale(scale, duration / 2));
        seq.SetLoops(-1);
        seq.Pause();

        return seq;
    }
    
    public static Sequence CreateHighlightEffect(Image image, Color color1, Color color2, float duration)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(image.DOColor(color1, duration / 2));
        seq.Append(image.DOColor(color2, duration / 2));
        seq.SetLoops(-1);
        seq.Pause();
        
        return seq;
    }
}