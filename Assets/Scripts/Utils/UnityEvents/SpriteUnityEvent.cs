﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class SpriteUnityEvent : UnityEvent<Sprite> {}
