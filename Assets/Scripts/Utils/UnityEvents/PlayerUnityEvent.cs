﻿using UnityEngine.Events;

[System.Serializable]
public class PlayerUnityEvent : UnityEvent<Player> {}