﻿using UnityEngine.Events;

[System.Serializable]
public class RecipeUnityEvent : UnityEvent<Recipe> {}