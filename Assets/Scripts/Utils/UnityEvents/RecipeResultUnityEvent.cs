﻿using UnityEngine.Events;

[System.Serializable]
public class RecipeResultUnityEvent : UnityEvent<RecipeResult> {}