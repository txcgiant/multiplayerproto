﻿using UnityEngine.Events;  

[System.Serializable] 
public class WaterPlaceUnityEvent : UnityEvent<WaterConsumer> {}