﻿using System.Collections.Generic;
using UnityEngine.Events;
 
[System.Serializable]
public class IngredientsUnityEvent : UnityEvent<List<Ingredient>> {}