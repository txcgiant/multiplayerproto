﻿using System.Collections.Generic;
using UnityEngine.Events;
 
[System.Serializable]
public class IngredientUnityEvent : UnityEvent<Ingredient> {}