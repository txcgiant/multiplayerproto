﻿using UnityEngine.Events;

[System.Serializable]
public class PlantTypeUnityEvent : UnityEvent<PlantType> {}