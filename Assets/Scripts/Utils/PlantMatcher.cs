﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/PlantsData")]
public class PlantMatcher : ScriptableObject
{
    public List<PlantData> plants;

    public Sprite GetSprite(PlantType type)
    {
        return Get(type)?.sprite;
    }

    public Ingredient GetIngredient(PlantType type)
    {
        return Get(type)?.ingredient;
    }

    public GameObject GetSaplingPrefab(PlantType type)
    {
        return Get(type)?.saplingPrefab;
    }

    public GameObject GetPlantPrefab(PlantType type)
    {
        return Get(type)?.plantPrefab;
    }

    private PlantData Get(PlantType type)
    {
        return plants.Find(p => p.type == type);
    }
}

[System.Serializable]
public class PlantData
{
    public PlantType type;
    public Ingredient ingredient;
    public Sprite sprite;
    public GameObject saplingPrefab;
    public GameObject plantPrefab;
}