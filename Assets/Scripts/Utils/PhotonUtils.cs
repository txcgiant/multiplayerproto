﻿
using UnityEngine;

public abstract class PhotonUtils
{   
    public static T GetComponentByViewId<T>(int id) where T : Component
    {
        return PhotonView.Find(id).GetComponent<T>(); 
    }
}
