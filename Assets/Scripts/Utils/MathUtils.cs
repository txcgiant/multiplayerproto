﻿using UnityEngine;

public static class MathUtils
{
    public static float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        float sign = (vec2.y < vec1.y)? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, vec2 - vec1) * sign;
    }
}