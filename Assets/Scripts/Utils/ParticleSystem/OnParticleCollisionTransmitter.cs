﻿using UnityEngine;

public class OnParticleCollisionTransmitter : MonoBehaviour
{
    private IOnParticleCollisionReceiver receiver;

    private void Awake()
    {
        receiver = transform.parent.GetComponent<IOnParticleCollisionReceiver>();
    }

    private void OnParticleCollision(GameObject other)
    {
        receiver.OnParticleCollision(other);
    }
}