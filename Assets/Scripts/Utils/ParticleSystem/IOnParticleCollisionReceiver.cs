﻿using UnityEngine;

public interface IOnParticleCollisionReceiver
{
    void OnParticleCollision(GameObject gameObject);
}