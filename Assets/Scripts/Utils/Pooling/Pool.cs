﻿using System.Collections.Generic;
using UnityEngine;
using Utils;

public class Pool<T> where T : IPoolable, new()
{
    private const int DefaultPoolSize = 10;
    private const int DefaultStackSize = 50;
    private readonly Stack<T> items = new Stack<T>(DefaultStackSize);

    public Pool() : this(DefaultPoolSize) {}
    
    public Pool(int initialSize)
    {
        for (int i = 0; i < initialSize; i++) {
            items.Push(new T());
        }
    }

    public T Get()
    {
        return items.Count == 0 ? new T() : items.Pop();
    }

    public void AddAll(IList<T> otherItems)
    {
        foreach (T item in otherItems) {
            Add(item);
        }
        otherItems.Clear();
    }

    public void Add(T item)
    {
        item.Reset();
        items.Push(item);
    }

    public Stack<T> GetStack()
    {
        return items;
    }
}