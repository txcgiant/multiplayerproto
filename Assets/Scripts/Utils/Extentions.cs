﻿using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public static class Extensions
    {
        public static T RemoveFirst<T>(this IList<T> list)
        {
            lock(list) {
                T first = list.First();
                list.RemoveAt(0);
                return first;
            }
        }
    }
}