﻿using JetBrains.Annotations;

public enum PlantType
{
    [UsedImplicitly] PLANT_ONE = 1,
    [UsedImplicitly] PLANT_TWO = 2,
    [UsedImplicitly] PLANT_WEED = 3,
    [UsedImplicitly] PLANT_FIRE = 4
}