﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LauncherBackgroundImageScaler : MonoBehaviour {

    private CanvasScaler background_canvasscaler;

    //Checks if adaption on runtime is needed
    public bool ScaleOnRuntime = true;


	void Start () {

    background_canvasscaler = GetComponent<CanvasScaler>();
    ScaleBackgroundUIAccordingToScreenAspectRatio();
        
	}

    void Update()
    {
        if (ScaleOnRuntime == true)
        {
            ScaleBackgroundUIAccordingToScreenAspectRatio();
        }
    }

    // If the setting is set to 0, the Canvas is scaled according to the 
    // difference between the current screen resolution width and the reference 
    // resolution width. If set to 1, the Canvas is scaled according to the difference height difference.

    private void ScaleBackgroundUIAccordingToScreenAspectRatio()
    {
        if (Screen.width <= Screen.height * 1.5)
        {
            background_canvasscaler.matchWidthOrHeight = 1;
        }
        else
        {
            background_canvasscaler.matchWidthOrHeight = 0;
        }
    }
}
