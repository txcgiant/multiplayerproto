﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class PendingMasterRecipesDisplay : MonoBehaviour
{
    public AdvanceStageAction cauldronBig;

    private void Awake()
    {
        DisplayRecipes();
    }

    [UsedImplicitly]
    public void RemoveTopRecipe()
    {
        Destroy(transform.GetChild(0).gameObject);
    }

    private void DisplayRecipes()
    {
        foreach (MasterRecipe masterRecipe in cauldronBig.masterRecipes) {
            DisplayRecipe(masterRecipe);
        }
    }

    private void DisplayRecipe(MasterRecipe masterRecipe)
    {
        GameObject container =
            Instantiate(Resources.Load("UI/Pending_Master_Recipes_Container", typeof(GameObject)) as GameObject,
                transform);
        
        foreach (var ingredientAmount in masterRecipe.ingredientAmounts) {
            for (int i = 0; i < ingredientAmount.amount; i++) {
                AddImageToUi(ingredientAmount.ingredient.image, container.transform);
            }
        }
    }
    
    private void AddImageToUi(Sprite image, Transform parent)
    {
        Instantiate(Resources.Load("UI/Ingredient_Image", typeof(GameObject)) as GameObject, parent)
            .GetComponent<Image>().sprite = image;
    }
}