﻿using System;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

public class TimerDisplay : MonoBehaviour
{
    private TextMeshProUGUI text;
    private float duration;
    
    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        duration = GameManager.instance.settings.playDuration;
    }

    [UsedImplicitly]
    public void UpdateTimer(float newTime)
    {
        TimeSpan t = TimeSpan.FromSeconds( (int)(duration - newTime));

        string answer = $"{t.Minutes:D2}:{t.Seconds:D2}";
            
        text.text = answer;
    }
}