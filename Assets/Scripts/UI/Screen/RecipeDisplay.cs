﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RecipeDisplay : MonoBehaviour
{
    [Header("FadeIn")]
    public Ease fadeInEase = Ease.InBounce;
    public float fadeInTime = 0.5f;
    [Header("FadeOut")]
    public Ease fadeOutEase = Ease.InBounce;
    public float fadeOutTime = 0.5f;
    
    private GameObject canvas;
    private InputManager inputManager;
    private bool canvasVisible;
    private GameObject visiblePositionGameObject;
    private GameObject invisiblePositionGameObject;

    private Vector2 VisiblePosition {
        get { return visiblePositionGameObject.transform.position; }
    }

    private Vector2 InvisiblePosition {
        get { return invisiblePositionGameObject.transform.position; }
    }

    private Coroutine hideRecipeCoroutine;
    private Tweener fadeIn;
    private Tweener fadeOut;

    private void Start()
    {
        inputManager = InputManager.instance;
        canvas = transform.GetChild(0).gameObject;


        foreach (Recipe recipe in GameManager.instance.recipeBook.recipes) {
            DisplayRecipe(recipe);
        }
        
        SetPositions();
    }

    private void SetPositions()
    {
        invisiblePositionGameObject = transform.parent.Find("Invisible Pos").gameObject;
        visiblePositionGameObject = transform.parent.Find("Visible Pos").gameObject;
        transform.position = InvisiblePosition;
    }

    private void Update()
    {
        if (inputManager.RecipeKeyDown) {
            if(!canvasVisible) {
                if (IsFadingIn()) {
                    HideRecipes();
                } else {
                    SchowRecipes();
                }
            } else if(canvasVisible) {
                if (IsFadingOut()) {
                    SchowRecipes();
                } else {
                    HideRecipes();
                }
            }
        }
    }

    private void SchowRecipes()
    {
        fadeOut?.Kill();
        
        transform.position = InvisiblePosition;
        
        hideRecipeCoroutine = StartCoroutine(HideRecipesInSeconds(5));
        fadeIn = transform
            .DOMove(VisiblePosition, fadeInTime)
            .OnComplete(() => { canvasVisible = true; })
            .SetEase(fadeInEase);
    }

    private bool IsFadingIn()
    {
        return fadeIn != null && fadeIn.IsPlaying();
    }
    
    private bool IsFadingOut()
    {
        return fadeOut != null && fadeOut.IsPlaying();
    }

    private IEnumerator HideRecipesInSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        HideRecipes();
    }

    private void HideRecipes()
    {
        fadeIn?.Kill();
        
        StopCoroutine(hideRecipeCoroutine);
        
        fadeOut = transform
            .DOMove(InvisiblePosition, fadeOutTime)
            .OnComplete(() => { canvasVisible = false; })
            .SetEase(fadeOutEase);;
    }
    
    private void DisplayRecipe(Recipe recipe)
    {
        GameObject recipeDisplay = Instantiate(Resources.Load("UI/Recipe_Display", typeof(GameObject)) as GameObject, canvas.transform);
        
        recipeDisplay.transform.Find("Result").GetComponent<Image>().sprite = recipe.recipeResult.ingredient.image;

        Transform ingredientParent = recipeDisplay.transform.Find("Ingredients");
        
        foreach (var ingredientAmount in recipe.ingredientAmounts) {
            for (int i = 0; i < ingredientAmount.amount; i++) {
                AddImage(ingredientAmount.ingredient.image, ingredientParent);
            }
        }
    }
    
    private static void AddImage(Sprite image, Transform parent)
    {
        Instantiate(Resources.Load("UI/Ingredient_Image", typeof(GameObject)) as GameObject, parent)
            .GetComponent<Image>().sprite = image;
    }
}