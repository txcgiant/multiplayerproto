﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class CurrentMasterRecipeDisplay : MonoBehaviour
{
    private readonly List<UiGameObject> uiGameObjects = new List<UiGameObject>();

    [UsedImplicitly]
    public void RemoveIngredient(Ingredient ingredient)
    {
        RemoveIngredientFromUi(ingredient);
    }

    [UsedImplicitly]
    public void DisplayNewRecipe(MasterRecipe masterRecipe)
    {
        RemoveAllImages();
        DisplayRecipe(masterRecipe);
    }

    [UsedImplicitly]
    public void ClearUi()
    {
        RemoveAllImages();
    }

    private void DisplayRecipe(MasterRecipe masterRecipe)
    {       
        foreach (var ingredientAmount in masterRecipe.ingredientAmounts) {
            for (int i = 0; i < ingredientAmount.amount; i++) {
                AddIngredientToUi(ingredientAmount.ingredient);
            }
        }
    }

    private void AddIngredientToUi(Ingredient ingredient)
    {
        GameObject image = AddImageToUi(ingredient.image);
        
        uiGameObjects.Add(new UiGameObject(ingredient.id, image));
    }
    
    private GameObject AddImageToUi(Sprite image)
    {
        GameObject gameObj = Instantiate(Resources.Load("UI/Ingredient_Image", typeof(GameObject)) as GameObject, transform);
        gameObj.GetComponent<Image>().sprite = image;

        return gameObj;
    }
    
    private void RemoveIngredientFromUi(Ingredient ingredient)
    {
        if (ingredient == null) {
            return;
        }

        UiGameObject uiGameObject = uiGameObjects.Find(p => p.id == ingredient.id);

        if (uiGameObject == null) {
            return;
        }

        uiGameObjects.Remove(uiGameObject);
        Destroy(uiGameObject.gameObject);
    }
    
    private void RemoveAllImages()
    {
        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }
        uiGameObjects.Clear();
    }
}

internal class UiGameObject
{
    public readonly int id;
    public readonly GameObject gameObject;

    public UiGameObject(int id, GameObject gameObject)
    {
        this.id = id;
        this.gameObject = gameObject;
    }
}