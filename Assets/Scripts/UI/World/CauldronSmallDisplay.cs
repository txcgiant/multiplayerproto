﻿using System.Collections.Generic;
using System.Linq.Expressions;
using DG.Tweening;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CauldronSmallDisplay : MonoBehaviour
{
    public Transform parentTransform;
    
    public Sprite defaultSprite;
    [Tooltip("Gameobject with a Grid Layout that arranges the Icons.")]
    public Transform iconParent;
    public IngredientContainer ingredientContainer;
    public TextMeshProUGUI timeDisplay;
    
    [Header("Fade In")]
    public Ease fadeInEase = Ease.OutBack;
    public float fadeInDuration = 0.2f;

    [Header("Timer")] 
    public float timerDuration = 1f;
    public float timerDistance = 20f;
    public Ease timerMoveEase = Ease.InSine;
    
    private Image[] images;
    private int capacity;
    
    private void Awake()
    {
        if (parentTransform != null) {
            transform.SetParent(parentTransform);
        }
        
        capacity = ingredientContainer.capacity;
        
        RegisterActions();
        InitImages();
        ShowIngredients(new List<Ingredient>());
        
    }

    private void RegisterActions()
    {
        ingredientContainer.onIngredientsChanged.AddListener(ShowIngredients);
        ingredientContainer.onIngredientsCleared.AddListener(ClearUi);
    }

    private void InitImages()
    {
        images = new Image[capacity];
        
        for (int i = 0; i < capacity; i++) {
            images[i] = Instantiate(
                Resources.Load("UI/Ingredient_Image", typeof(GameObject)) as GameObject, iconParent).GetComponent<Image>();
        }
    }

    [UsedImplicitly]
    public void ShowIngredients(List<Ingredient> ingredients)
    {
        for (int i = 0; i < ingredients.Count; i++) {
            AddIngredientImage(images[i], ingredients[i]);
        }
    }
    
    [UsedImplicitly]
    public void ClearUi()
    {
        foreach (Image image in images) {
            image.gameObject.SetActive(true);
            image.sprite = defaultSprite;
        }
    }

    [UsedImplicitly]
    public void ShowResult(RecipeResult recipeResult)
    {
        foreach (Image image in images) {
            image.gameObject.SetActive(false);
        }
        
        images[1].gameObject.SetActive(true);
        AddIngredientImage(images[1], recipeResult.ingredient);
    }

    [UsedImplicitly]
    public void HideIcons()
    {
        iconParent.gameObject.SetActive(false);
    }
    
    [UsedImplicitly]
    public void ShowIcons()
    {
        iconParent.gameObject.SetActive(true);
    }

    [UsedImplicitly]
    public void DisplayTime(float value)
    {
        timeDisplay.gameObject.SetActive(true);
        timeDisplay.text = "+" + value;
        timeDisplay.transform
            .DOLocalMoveY(timerDistance, timerDuration)
            .SetEase(timerMoveEase)
            .OnComplete(ResetTimeDisplay);
    }

    private void ResetTimeDisplay()
    {
        timeDisplay.gameObject.SetActive(false);
        timeDisplay.transform.DOLocalMoveY(-timerDistance, 0);
    }

    private void AddIngredientImage(Image image, Ingredient ingredient)
    {
        if (IsImageAlreadySet(image, ingredient)) {
            return;
        }
        
        image.sprite = ingredient.image;

        HighlightIngredient(image);
    }

    private bool IsImageAlreadySet(Image image, Ingredient ingredient)
    {
        return image.sprite == ingredient.image;
    }

    private void HighlightIngredient(Image image)
    {
        image.transform.localScale = Vector3.zero;
        image.transform
            .DOScale(1f, fadeInDuration)
            .SetEase(fadeInEase);
    }
}
