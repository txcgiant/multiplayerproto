﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using Image = UnityEngine.UI.Image;

public class SaplingSelectionDisplay : MonoBehaviour
{
    [Tooltip("Radius depends on screen height. Default: 0.1")]
    public float radius = 0.1f;
    
    [Header("Fade In")]
    public Ease fadeInEase = Ease.OutBack;
    public float fadeInDuration = 0.2f;
    
    private Planting target;
    private Pool<Section> pool;
    private List<Section> sections;

    private GameObject directionMarker;
    private Section selected;

    private InputManager input;

    public PlantTypeUnityEvent onSelectionDone;

    private void Awake()
    {
        pool = new Pool<Section>(5);
        sections = new List<Section>();

        foreach (Section item in pool.GetStack()) {
            item.GameObject.transform.SetParent(transform);
            item.GameObject.SetActive(false);
        }

        directionMarker = new GameObject("DirectionMarker");
        directionMarker.transform.SetParent(transform);
    }

    private void OnEnable()
    {
        input = InputManager.instance;
    }
    
    private void Update()
    {
        if (sections.Count == 0) {
            return;
        }
        
        Section section = GetSection();
        if (selected == section) {
            return;
        }

        selected?.Highlight(false);
        selected = section;
        selected?.Highlight(true);
    }

    private void LateUpdate()
    {
        if (target != null) {
            transform.position = Camera.main.WorldToScreenPoint(target.transform.position);
        }
    }

    public void Interact()
    {
        if (selected != null) {
            onSelectionDone?.Invoke(selected.GetPlantType());
        }
    }

    public void Show(Planting target)
    {
        this.target = target;
        CreateMenu(this.target.possiblePlants);
        gameObject.SetActive(true);
        gameObject.transform.localScale = Vector3.zero;
        gameObject.transform.DOScale(1, fadeInDuration).SetEase(fadeInEase);
    }

    public void Hide()
    {
        target = null;
        pool.AddAll(sections);
        gameObject.SetActive(false);   
        selected = null;
    }
    
    private void CreateMenu(List<PlantType> plants)
    {
        float angle = GetStartAngle(plants.Count);
        float range = GetDistanceBetweenSections(plants.Count);
            
        foreach (PlantType type in plants) {

            Section section = pool.Get().Set(
                type,
                GetPositionOnCircle(radius, angle),
                transform
            );
            
            sections.Add(section);

            angle += range;
        }
    }

    private float GetDistanceBetweenSections(int plantCount) => 2 * Mathf.PI / plantCount;
    private float GetStartAngle(int plantCount) => plantCount > 2 ? GetUpAngle(): 0;
    private float GetUpAngle() => -(3 * Mathf.PI / 2);

    private Section GetSection()
    {
        float angle = CalculateAngle();
        
        // if no input is detected return the last selected
        if (!input.IsMouseMoving() && !input.IsLeftStickMoving()) {
            return selected;
        }
        
        directionMarker.transform.position = GetPositionOnCircle(radius * 3, angle * Mathf.Deg2Rad);
        
        SortSectionsByDistanceToDirectionMarker();
        
        return sections.First();
    }

    private void SortSectionsByDistanceToDirectionMarker()
    {
        sections.Sort((a,b) => CompareDistanceToMarker(a.Position, b.Position));
    }

    private int CompareDistanceToMarker(Vector3 vec1, Vector3 vec2)
    {
        return Vector3.Distance(vec1, directionMarker.transform.position)
            .CompareTo(Vector3.Distance(vec2, directionMarker.transform.position));
    }

    private float CalculateAngle()
    {
        return input.IsMouseMoving() ? 
            MathUtils.AngleBetweenVector2(transform.position, Input.mousePosition) :
            MathUtils.AngleBetweenVector2(
                Vector2.zero,
                input.LeftStick);
    }
    
    private Vector2 GetPositionOnCircle(float radius, float angle)
    {
        // Random.onUnitSphere TODO: USE THIS IDIOT
        radius *= Screen.height;
        float x = radius * Mathf.Cos(angle);
        float y = radius * Mathf.Sin(angle);
        
        return new Vector2(transform.position.x + x, transform.position.y + y);
    }
    
    // #################
    // ## Inner class ##
    // #################
    
    public class Section : IPoolable
    {
        public Vector3 Position => gameObject.transform.position;
        public GameObject GameObject => gameObject.gameObject;
        
        private readonly GameObject gameObject;
        private readonly Image image;

        private PlantType type;
    
        public Section()
        {
            gameObject = new GameObject();
            image = gameObject.AddComponent<Image>();
        }

        public Section Set(PlantType type, Vector2 position, Transform parent)
        {
            this.type = type;
            image.sprite = GameManager.instance.plantMatcher.GetSprite(type);

            gameObject.transform.position = position;
            gameObject.transform.SetParent(parent);
            gameObject.transform.localScale = Vector3.one;
            gameObject.name = type.ToString();
            gameObject.SetActive(true);
        
            return this;
        }

        public PlantType GetPlantType()
        {
            return type;
        }

        public void Reset()
        {
            gameObject.SetActive(false);
            image.sprite = null;
        }

        public void Highlight(bool highlight)
        {
            gameObject.transform.DOScale(highlight ? 1.4f : 1, 0.2f)
                .SetEase(Ease.OutBack);
        }

        public override string ToString()
        {
            return gameObject.name;
        }
    }
}
