﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Slider slider;

    [UsedImplicitly]
    public void ShowProgressBar()
    {
        slider.gameObject.SetActive(true);
    }

    [UsedImplicitly]
    public void HideProgressBar()
    {
        slider.gameObject.SetActive(false);
    }

    [UsedImplicitly]
    public void SetAmount(float percentage)
    {
        slider.value = percentage;
    }
}