﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarPicture : MonoBehaviour
{
    public GameObject progressBar;
    
    [Header("Image")]
    public Image image;
    public bool wobbleEffect = false;
    public Ease fadeInEase = Ease.OutBack;
    public float fadeInDuration = 0.2f;

    private Slider slider;

    private void Awake()
    {
        slider = progressBar.GetComponent<Slider>();
    }

    public void Start()
    {
        if (wobbleEffect) {
            Effects.CreateWobbleEffect(image.transform, 0.8f, 1.2f).Play();
        }
    }

    [UsedImplicitly]
    public void PictureShow()
    {
        image.gameObject.SetActive(true);
        HighlightImage(image);
    }
    
    [UsedImplicitly]
    public void PictureHide()
    {
        image.gameObject.SetActive(false);
    }

    [UsedImplicitly]
    public void PictureShowIfNotStarted()
    {
        if (!HasStarted()) {
            PictureShow();
        }
    }

    [UsedImplicitly]
    public void PictureSet(Sprite sprite)
    {
        image.sprite = sprite;
    }

    [UsedImplicitly]
    public void PictureMoveLocalYPosition(float position)
    {
        image.transform.DOLocalMoveY(position, 0.2f).SetEase(Ease.InOutSine);
    }
    
    [UsedImplicitly]
    public void PictureMoveLocalXPosition(float position)
    {
        image.transform.DOLocalMoveX(position, 0.2f).SetEase(Ease.InOutSine);
    }

    [UsedImplicitly]
    public void PictureSetSize(float size)
    {
        image.rectTransform.DOSizeDelta(new Vector2(size, size), 0.2f);
    }

    [UsedImplicitly]
    public void PictureJumpToCenterAndRotate()
    {
        image.transform.DOLocalJump(Vector3.zero, 150f, 1, 1.5f);
        image.transform.DORotate(new Vector3(360, 0, 0), .28f, RotateMode.FastBeyond360).SetLoops(5, LoopType.Incremental).SetRelative().SetEase(Ease.InOutSine);
    }
    
    [UsedImplicitly]
    public void ProgressBarHideIfNotStarted()
    {
        if (!HasStarted()) {
            ProgressBarHide();
        }
    }

    [UsedImplicitly]
    public void ProgressBarShow()
    {
        progressBar.SetActive(true);
    }

    [UsedImplicitly]
    public void ProgressBarHide()
    {
        progressBar.SetActive(false);
    }

    [UsedImplicitly]
    public void ProgressBarSetValue(float percentage)
    {
        slider.value = percentage;
    }

    [UsedImplicitly]
    public void tProgressBarReseValue()
    {
        ProgressBarSetValue(0);
    }

    private bool HasStarted()
    {
        return slider.value > 0;
    }
    
    private void HighlightImage(Image image)
    {
        image.transform.localScale = Vector3.zero;
        image.transform
            .DOScale(1f, fadeInDuration)
            .SetEase(fadeInEase);
    }
}
