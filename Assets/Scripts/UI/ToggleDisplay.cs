﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ToggleDisplay : Toggle
{
    public Action<bool> onClick;
    
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        if (!interactable) {
            return;
        }
        if (onClick != null) {
            onClick.Invoke(isOn);
        }
    }
}