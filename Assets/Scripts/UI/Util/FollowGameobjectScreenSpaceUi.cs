﻿using UnityEngine;

public class FollowGameobjectScreenSpaceUi : MonoBehaviour
{
    public GameObject followObject;
    public float yOffset = 0.175f;
    
    private Camera uiCamera;
    
    public void Start()
    {
        uiCamera = Camera.main;
    }
    
    private void LateUpdate()
    {
        transform.position = uiCamera.WorldToScreenPoint(followObject.transform.position) + new Vector3(0, yOffset * Screen.height);
    }
}