﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLobbyDisplay : MonoBehaviour
{
    public Image background;
    public TextMeshProUGUI playerName;
    public ToggleDisplay playerReady;

    public void Reset()
    {
        gameObject.SetActive(false);
        background.color = Color.white;
        playerReady.isOn = false;
    }
}
