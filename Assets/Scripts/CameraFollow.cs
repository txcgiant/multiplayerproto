﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public Transform target;

	public float lerp = 0.125f;
	public Vector3 positionOffset = new Vector3(0, 25, -20);

	private float currentTime;
	private Vector3 targetPosition;

	private float startLerp = 10f;

	private void Update()
	{
		if (target == null) {
			return;
		}

		if (startLerp >= lerp) {
			startLerp -= 0.1f;
		}


		targetPosition = target.position + positionOffset;

		transform.LookAt(target);
 
		transform.position = Vector3.Lerp(transform.position, targetPosition, startLerp);

	}
}