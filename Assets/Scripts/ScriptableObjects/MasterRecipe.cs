﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions.Comparers;

[CreateAssetMenu(menuName = "MasterRecipe")]
public class MasterRecipe : ScriptableObject
{
    public List<IngredientAmount> ingredientAmounts;
    public MasterResult result;

    public MasterResult GetResult(List<Ingredient> plants)
    {
        foreach (IngredientAmount ingredientAmount in ingredientAmounts) {
            if (ingredientAmount.amount != plants.FindAll(p => p.id == ingredientAmount.ingredient.id).Count) {
                return null;
            }
        }

        return result;
    }
}

[System.Serializable]
public class MasterResult
{
    [Header("Cauldron")]
    public Color albedo;
    public Color emissionColor;
    public float smoothness;

    [Header("Light")] 
    public Color lightColor;
    public float lightIntensity;
}
