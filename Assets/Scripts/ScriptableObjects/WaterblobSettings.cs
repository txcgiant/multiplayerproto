﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/WaterblobSettings")]
public class WaterblobSettings : ScriptableObject
{
    [Header("Waterblob")]
    public int waterCapacity;
    public int waterLossPerSecond;
}