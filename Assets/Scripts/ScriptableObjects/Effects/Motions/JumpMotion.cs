﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "Motions/JumpMotion")]
public class JumpMotion : ScriptableObject
{
    public float duration;
    public float power = 10;
    public int amount = 2;
    public Ease ease;
    
    public Tween Play(Transform transform, Vector3 targetPoint)
    {
        Debug.Log(targetPoint);
        return transform
            .DOLocalJump(targetPoint, power, amount, duration).SetEase(ease);
    }
}