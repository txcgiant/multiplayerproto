﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "Motions/LinearMotion")]
public class LinearMotion : ScriptableObject
{
    public float duration;
    public Ease ease;
    
    public Tween Play(Transform transform, Vector3 targetPoint)
    {
        return transform
            .DOMove(targetPoint, duration).SetEase(ease);
    }
}