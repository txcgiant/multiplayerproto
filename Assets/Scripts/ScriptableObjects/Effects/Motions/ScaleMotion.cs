﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "Motions/ScaleMotion")]
public class ScaleMotion : ScriptableObject
{
    public float duration = 0.2f;
    public Vector3 scale = new Vector3(1f, 0.3f, 1f);
    public Ease ease;
    
    public Tween Play(Transform transform)
    {
        return transform.DOScale(scale, duration).SetEase(ease);
    }
}