﻿using DG.Tweening;
using UnityEngine;


[CreateAssetMenu(menuName = "Motions/DropMotion")]
public class DropMotion : ScriptableObject
{
    [Header("FallStart")] public ScaleMotion start;
    [Header("Fall")] public LinearMotion drop;
    [Header("Hit")] public ScaleMotion end;

    public Tween Play(Transform transform, Vector3 targetPoint, TweenCallback onHitTheGround)
    {
        Vector3 scale = transform.localScale;
        
        Sequence falling = DOTween.Sequence();
        Tween t = drop.Play(transform, targetPoint);
        
        falling.Insert(0f, start.Play(transform));
        falling.Insert(0f, t);

        t.OnComplete(() =>
        {
            Sequence seq = DOTween.Sequence();
            seq.Append(end.Play(transform));
            seq.Append(transform.DOScale(scale, 0.1f));
            
            onHitTheGround?.Invoke();
        });

        return falling;
    }
}