﻿using UnityEngine;

[CreateAssetMenu(menuName = "Ingredient")]
public class Ingredient : ScriptableObject
{
    public int id;
    public Sprite image;
}