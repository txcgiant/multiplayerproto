﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/BedAppearance")]
public class BedAppearanceSettings : ScriptableObject
{
    [Header("Dry")] 
    public Color dryColor;

    [Header("Watered")] 
    public Color wetColor;
}