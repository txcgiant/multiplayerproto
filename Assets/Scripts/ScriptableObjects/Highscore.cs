﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Highscore")]
public class Highscore : ScriptableObject
{
    [SerializeField]
    public List<HighscoreElement> elements;

    public void Add(string teamname, int score)
    {
        elements.Add(new HighscoreElement(teamname, score));
        
        elements.Sort((e1,e2) => e1.score.CompareTo(e2.score));
    }
    
}

[System.Serializable]
public class HighscoreElement
{
    public string teamName;
    public int score;

    public HighscoreElement(string teamName, int score)
    {
        this.teamName = teamName;
        this.score = score;
    }
}