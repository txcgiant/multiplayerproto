﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/PlayerColorMaterials")]
public class PlayerColorMaterials : ScriptableObject
{
    public Material[] materials;
}