﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "RecipeBook")]
public class RecipeBook : ScriptableObject
{
    public Recipe[] recipes;
    public RecipeResult[] trashRecipeResults;
    
    public RecipeResult GetRecipe(List<Ingredient> plants)
    {
        return recipes
               .Select(recipe => recipe.GetResult(plants))
               .FirstOrDefault(result => result != null) ?? GetRandomTrashResult();
    }

    private RecipeResult GetRandomTrashResult()
    {
        return trashRecipeResults[Random.Range(0, trashRecipeResults.Length)];
    }
}