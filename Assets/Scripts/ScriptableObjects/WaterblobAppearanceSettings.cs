﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/WaterblobAppearanceSettings")]
public class WaterblobAppearanceSettings : ScriptableObject
{
    public float dropletsRadius = 0.175f;
    public float emissionOverDistanceMin;
    public float emissionOverDistanceMax;
    
    [Header("Normal")]
    public float normalEmissionOverTimeMin;
    public float normalEmissionOverTimeMax;
    
    [Header("Watering")]
    public float wateringEmissionOverTimeMin;
    public float wateringEmissionOverTimeMax;
}