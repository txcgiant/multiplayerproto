﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Recipe")]
public class Recipe : ScriptableObject
{
    public List<IngredientAmount> ingredientAmounts;
    public List<Ingredient> ingredientsSorted;
    public RecipeResult recipeResult;

    public RecipeResult GetResult(List<Ingredient> plants)
    {
        if (IsPerfectResult(plants)) {
            recipeResult.isPerfect = true;
            return recipeResult.Copy();
        } 
        
        recipeResult.isPerfect = false;
        
        foreach (IngredientAmount ingredientAmount in ingredientAmounts) {
            if (ingredientAmount.amount != plants.FindAll(p => p.id == ingredientAmount.ingredient.id).Count) {
                return null;
            }
        }

        return recipeResult.Copy();
    }

    private bool IsPerfectResult(List<Ingredient> plants)
    {
        if (ingredientsSorted.Count == 0) {
            InitIngridientsSorted();
        }

        if (plants.Count != ingredientsSorted.Count) {
            return false;
        }

        for (int i = 0; i < plants.Count; i++) {
            if (plants[i].id != ingredientsSorted[i].id) {
                return false;
            }
        }
        
        return true;
    }

    private void InitIngridientsSorted()
    {
        ingredientsSorted = new List<Ingredient>();

        foreach (IngredientAmount ingredientAmount in ingredientAmounts) {
            for (int i = 0; i < ingredientAmount.amount; i++) {
                ingredientsSorted.Add(ingredientAmount.ingredient);
            }
        }
    }
}

[System.Serializable]
public class IngredientAmount
{
    public Ingredient ingredient;
    public int amount;
}

[System.Serializable]
public class RecipeResult
{
    public Ingredient ingredient;
    public Color color;
    public Color smokeEmissionColor;
    public float brewDuration = 1f;
    public float timeBonusIfPerfect = 15f;
    [NonSerialized]
    public bool isPerfect;

    public RecipeResult(Ingredient ingredient, Color color, Color smokeEmissionColor, float brewDuration, bool isPerfect) {
        this.ingredient = ingredient;
        this.color = color;
        this.smokeEmissionColor = smokeEmissionColor;
        this.brewDuration = brewDuration;
        this.isPerfect = isPerfect;
    }

    public RecipeResult Copy()
    {
        return new RecipeResult(ingredient, color, smokeEmissionColor, brewDuration, isPerfect);
    }
}
