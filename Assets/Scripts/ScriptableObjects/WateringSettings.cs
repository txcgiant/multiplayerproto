﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/WateringSettings")]
public class WateringSettings : ScriptableObject
{
    [Header("Watering")]
    public int wateringAmountPerTick;
}