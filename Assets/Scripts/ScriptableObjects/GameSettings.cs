﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/GameSettings")]
public class GameSettings : ScriptableObject
{
    public float playDuration = 240;
    public float gameOverDelay = 1;
    
    public string launchSceneName;
    public string gameSceneName;
    public string gameOverSceneName;
    public PlayerColorMaterials playerColorMaterials;
}