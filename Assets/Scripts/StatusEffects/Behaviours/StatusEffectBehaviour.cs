﻿using UnityEngine;

public abstract class StatusEffectBehaviour : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        StatusEffectsHolder statusEffectsHolder = 
            other.gameObject.GetComponent<StatusEffectsHolder>();

        if (statusEffectsHolder == null) {
            return;
        }

        statusEffectsHolder.Apply(GetSettings());
    }

    protected abstract AbstractStatusEffectSettings GetSettings();
}