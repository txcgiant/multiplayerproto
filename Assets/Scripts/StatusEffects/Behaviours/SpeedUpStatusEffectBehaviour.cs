﻿public class SpeedUpStatusEffectBehaviour : ImpairingMovementStatusEffectBehaviour
{
    public SpeedUpStatusEffectSettings settings;

    protected override AbstractStatusEffectSettings GetSettings()
    {
        return settings;
    }
}