﻿public class SlowStatusEffectBehaviour : ImpairingMovementStatusEffectBehaviour
{
    public SlowStatusEffectSettings settings;

    protected override AbstractStatusEffectSettings GetSettings()
    {
        return settings;
    }
}