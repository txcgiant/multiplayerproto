﻿public class RootStatusEffectBehaviour : StatusEffectBehaviour
{
    public RootStatusEffectSettings settings;

    protected override AbstractStatusEffectSettings GetSettings()
    {
        return settings;
    }
}