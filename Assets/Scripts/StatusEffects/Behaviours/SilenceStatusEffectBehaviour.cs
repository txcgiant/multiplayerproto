﻿public class SilenceStatusEffectBehaviour : StatusEffectBehaviour
{
    public SilenceStatusEffectSettings settings;

    protected override AbstractStatusEffectSettings GetSettings()
    {
        return settings;
    }
}