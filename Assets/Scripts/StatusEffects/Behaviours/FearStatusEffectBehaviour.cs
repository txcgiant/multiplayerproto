﻿public class FearStatusEffectBehaviour : StatusEffectBehaviour
{
    public FearStatusEffectSettings settings;
    
    protected override AbstractStatusEffectSettings GetSettings()
    {
        return settings;
    }
}