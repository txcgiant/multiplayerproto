﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "StatusEffects/SpeedUp")]
public class SpeedUpStatusEffectSettings : ImpairingMovementStatusEffectSettings
{
    public override StatusEffectFlag GetFlag()
    {
        return StatusEffectFlag.SPEED_UP;
    }
}