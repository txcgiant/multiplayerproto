﻿using System.Linq;
using UnityEngine;

public abstract class ImpairingMovementStatusEffectSettings : AbstractStatusEffectSettings
{
    public float speedMultiplier = 0.5f;
    
    public override AbstractStatusEffectStrategy GetStrategy(GameObject target, AbstractStatusEffectSettings settings)
    {
        IControlImpairingSpeedProvider[] providers = settings.GetProviders<IControlImpairingSpeedProvider>(target);
        return providers.Length == 0 ? null : new ImpairingMovementStatusEffectStrategy(providers,
            (ImpairingMovementStatusEffectSettings)settings);
    }
}