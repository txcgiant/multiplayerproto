﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "StatusEffects/Ignite")]
public class FearStatusEffectSettings : AbstractStatusEffectSettings
{
    public float fearRadius = 2f;
    public override AbstractStatusEffectStrategy GetStrategy(GameObject target, AbstractStatusEffectSettings settings)
    {
        IControlFearProvider[] providers = settings.GetProviders<IControlFearProvider>(target);
        return providers.Length == 0 ? null : new FearStatusEffectStrategy(providers, (FearStatusEffectSettings)settings);
    }

    public override StatusEffectFlag GetFlag()
    {
        return StatusEffectFlag.FEAR;
    }
}