﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "StatusEffects/Silence")]
public class SilenceStatusEffectSettings : AbstractStatusEffectSettings
{
    public override AbstractStatusEffectStrategy GetStrategy(GameObject target, AbstractStatusEffectSettings settings)
    {
        IControlSilenceProvider[] providers = settings.GetProviders<IControlSilenceProvider>(target);
        return providers.Length == 0 ? null : new SilenceStatusEffectStrategy(providers);
    }

    public override StatusEffectFlag GetFlag()
    {
        return StatusEffectFlag.SILENCE;
    }
}