﻿using UnityEngine;

public abstract class AbstractStatusEffectSettings : ScriptableObject
{
    public int Id { get; set; }
    
    public float duration;
    public bool isUnique;
    
    public GameObject onStartFxPrefab;
    public GameObject onEndFxPrefab;

    public GameObject CreateStartFx(Transform parent)
    {
        return CreateFx(onStartFxPrefab, parent);
    }
    
    public GameObject CreateEndFx(Transform parent)
    {
        return CreateFx(onEndFxPrefab, parent);
    }

    private GameObject CreateFx(GameObject prefab, Transform parent)
    {
        if (prefab == null) {
            return null;
        }
        
        GameObject obj = Instantiate(prefab, parent);
        Destroy(obj, duration);
        
        return obj;
    }

    public abstract AbstractStatusEffectStrategy GetStrategy(GameObject target, AbstractStatusEffectSettings settings);
    public abstract StatusEffectFlag GetFlag();

    public T[] GetProviders<T>(GameObject target) where T : IControlProvider
    {
        return target.GetComponents<T>();
    }
}