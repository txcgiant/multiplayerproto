﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "StatusEffects/Slow")]
public class SlowStatusEffectSettings : ImpairingMovementStatusEffectSettings
{
    public override StatusEffectFlag GetFlag()
    {
        return StatusEffectFlag.SLOW;
    }
}