﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "StatusEffects/Root")]
public class RootStatusEffectSettings : AbstractStatusEffectSettings
{
    public override AbstractStatusEffectStrategy GetStrategy(GameObject target, AbstractStatusEffectSettings settings)
    {
        IControlRootProvider[] providers = settings.GetProviders<IControlRootProvider>(target);
        return providers.Length == 0 ? null : new RootStatusEffectStrategy(providers);
    }

    public override StatusEffectFlag GetFlag()
    {
        return StatusEffectFlag.ROOT;
    }
}