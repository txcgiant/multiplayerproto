﻿public interface IControlRootProvider : IControlProvider
{
    void StartRoot();
    void EndRoot();
}