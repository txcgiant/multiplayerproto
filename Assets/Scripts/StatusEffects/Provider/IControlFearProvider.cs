﻿public interface IControlFearProvider : IControlProvider
{
    void StartFear(float radius);
    void StopFear();
}