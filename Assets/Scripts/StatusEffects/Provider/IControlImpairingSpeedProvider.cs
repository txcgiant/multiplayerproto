﻿public interface IControlImpairingSpeedProvider : IControlProvider
{
    void SpeedUp(float multiplier);
    void SlowDown(float multiplier);
}