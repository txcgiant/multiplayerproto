﻿public interface IControlSilenceProvider : IControlProvider
{
    void OnSilenceStart();
    void OnSilenceEnd();
}