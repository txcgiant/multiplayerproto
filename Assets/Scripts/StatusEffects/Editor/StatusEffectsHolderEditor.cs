﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StatusEffectsHolder))]
public class StatusEffectsHolderEditor : Editor
{
    private StatusEffectsHolder holder;
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        holder = (StatusEffectsHolder) target;

        GUILayout.BeginHorizontal();

        int count = 0;
        foreach (var settings in StatusEffects.GetInstance().GetStatusEffectSettings()) {

            if (count % 4 == 0) {
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
            }
            
            if (GUILayout.Button(settings.name)) {
                holder.Apply(settings);
            }

            count++;
        }
        GUILayout.EndHorizontal();
        
        if (GUILayout.Button("RemoveAll")) {
            holder.RemoveAll();
        }
    }
}