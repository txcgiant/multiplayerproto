﻿using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractStatusEffectStrategy : IStatusEffectStrategy
{
    public virtual void OnTick(float delta) { }
    public virtual void OnStart() { }
    public virtual void OnEnd() { }

}