﻿using UnityEngine;

public class ImpairingMovementStatusEffectStrategy : AbstractStatusEffectStrategy
{
    private readonly IControlImpairingSpeedProvider[] providers;
    private readonly ImpairingMovementStatusEffectSettings settings;

    public ImpairingMovementStatusEffectStrategy(IControlImpairingSpeedProvider[] providers, ImpairingMovementStatusEffectSettings settings)
    {
        this.providers = providers;
        this.settings = settings;
    }
    
    public override void OnStart()
    {
        foreach (IControlImpairingSpeedProvider provider in providers) {
            provider.SpeedUp(settings.speedMultiplier);
        }
    }
    
    public override void OnEnd()
    {
        foreach (IControlImpairingSpeedProvider provider in providers) {
            provider.SlowDown(settings.speedMultiplier);
        }
    }
}
