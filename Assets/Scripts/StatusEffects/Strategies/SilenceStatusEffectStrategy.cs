﻿public class SilenceStatusEffectStrategy : AbstractStatusEffectStrategy
{
    private readonly IControlSilenceProvider[] providers;

    public SilenceStatusEffectStrategy(IControlSilenceProvider[] providers)
    {
        this.providers = providers;
    }
    
    public override void OnStart()
    {
        foreach (IControlSilenceProvider provider in providers) {
            provider.OnSilenceStart();
        }
    }
    
    public override void OnEnd()
    {
        foreach (IControlSilenceProvider provider in providers) {
            provider.OnSilenceEnd();
        }
    }
}