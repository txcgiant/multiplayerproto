﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class FearStatusEffectStrategy : AbstractStatusEffectStrategy
{
    private readonly IControlFearProvider[] providers;
    private readonly FearStatusEffectSettings settings;

    public FearStatusEffectStrategy(IControlFearProvider[] providers, FearStatusEffectSettings settings)
    {
        this.providers = providers;
        this.settings = settings;
    }
    
    public override void OnStart()
    {
        foreach (IControlFearProvider provider in providers) {
            provider.StartFear(settings.fearRadius);
        }
    }
    
    public override void OnEnd()
    {
        foreach (IControlFearProvider provider in providers) {
            provider.StopFear();
        }
    }
}

public class FearMovementStrategy : IMovementStrategy
{
    private Vector3 center;
    private readonly float radius;
    
    public FearMovementStrategy(Vector3 center, float radius)
    {
        this.center = center;
        this.radius = radius;
    }
    
    public void OnDestinationReached(Agent agent)
    {
        agent.SetDestination(RandomNavmeshLocation(radius, center));
    }

    public void OnStart(Agent agent)
    {
        center = agent.transform.position;
        agent.SetDestination(RandomNavmeshLocation(radius, center));
    }

    private Vector3 RandomNavmeshLocation(float radius, Vector3 center) {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += center;
        
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1)) {
            finalPosition = hit.position;            
        }
        return finalPosition;
    }

    public void OnStop(Agent agent)
    {
    }

    public float GetMinDistanceToDestination()
    {
        return 0.5f;
    }
}