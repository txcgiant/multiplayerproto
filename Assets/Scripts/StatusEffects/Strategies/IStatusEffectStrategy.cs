﻿public interface IStatusEffectStrategy
{
    void OnTick(float delta);
    void OnStart();
    void OnEnd();
}