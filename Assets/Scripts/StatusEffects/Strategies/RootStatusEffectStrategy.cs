﻿public class RootStatusEffectStrategy : AbstractStatusEffectStrategy
{
    private readonly IControlRootProvider[] providers;

    public RootStatusEffectStrategy(IControlRootProvider[] providers)
    {
        this.providers = providers;
    }
    
    public override void OnStart()
    {
        foreach (IControlRootProvider provider in providers) {
            provider.StartRoot();
        }
    }
    
    public override void OnEnd()
    {
        foreach (IControlRootProvider provider in providers) {
            provider.EndRoot();
        }
    }
}