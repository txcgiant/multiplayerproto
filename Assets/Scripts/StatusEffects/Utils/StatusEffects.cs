﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class StatusEffects
{
    private static StatusEffects instance;
    private readonly Dictionary<int, AbstractStatusEffectSettings> statusEffectSettings;

    public static StatusEffects GetInstance()
    {
        return instance ?? (instance = new StatusEffects());
    }

    public static void Init()
    {
        GetInstance().LoadAllAbstractStatusEffects();
    }

    private StatusEffects()
    {
        statusEffectSettings = new Dictionary<int, AbstractStatusEffectSettings>();
    }

    private void LoadAllAbstractStatusEffects()
    {
        int id = 1;
        foreach (AbstractStatusEffectSettings settings in Resources.LoadAll<AbstractStatusEffectSettings>("StatusEffects")) {
            settings.Id = id++;
            statusEffectSettings.Add(settings.Id, settings);
        }
    }

    public AbstractStatusEffectSettings GetSettingsByGuid(int id)
    {
        return statusEffectSettings[id];
    }

    public AbstractStatusEffectSettings[] GetStatusEffectSettings()
    {
        return statusEffectSettings.Values.ToArray();
    }
}