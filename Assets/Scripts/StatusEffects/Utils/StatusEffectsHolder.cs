﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Utils;

public class StatusEffectsHolder : Photon.MonoBehaviour
{
    public List<StatusEffectFlag> statusEffectMask;
    
    [SerializeField]
    private List<StatusEffect> statusEffects;

    private StatusEffectFlag flags;
    
    private void Awake()
    {
        statusEffects = new List<StatusEffect>();
    }

    public bool HasEffect(StatusEffectFlag flag)
    {
        return FlagsHelper.IsSet(flags, flag);
    }
 
    public void Apply(AbstractStatusEffectSettings settings)
    {
        if (PhotonNetwork.offlineMode) {
            RPC_StatusEffectsHolder_Apply(settings.Id);
            
        } else {
            photonView.RPC("RPC_StatusEffectsHolder_Apply", PhotonTargets.All, settings.Id);
        }
    }

    public void RemoveAll()
    {
        foreach (StatusEffect statusEffect in statusEffects.ToArray()) {
            Remove(statusEffect.Settings);
        }
    }

    public void Remove(AbstractStatusEffectSettings settings)
    {
        if (PhotonNetwork.offlineMode) {
            RPC_StatusEffectsHolder_Remove(settings.Id);
            
        } else {
            photonView.RPC("RPC_StatusEffectsHolder_Remove", PhotonTargets.All, settings.Id);
        }
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_StatusEffectsHolder_Remove(int id)
    {
        RemoveEffect(statusEffects.Find(effect => effect.Settings.Id == id));
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_StatusEffectsHolder_Apply(int id)
    {
        AbstractStatusEffectSettings settings = StatusEffects.GetInstance().GetSettingsByGuid(id);
        
        if (!IsApplyable(settings)) {
            return;
        }

        if (RefreshUniqueStatusEffect(settings)) {
            settings.CreateStartFx(transform);
            return;
        }

        AbstractStatusEffectStrategy strategy = settings.GetStrategy(gameObject, settings);

        if (strategy == null) {
            return;
        }
        
        StatusEffect statusEffect = new StatusEffect(gameObject, settings, strategy);
        
        settings.CreateStartFx(transform);
        statusEffect.onDone += OnDone;
        
        statusEffects.Add(statusEffect);
        statusEffect.Start();
        FlagsHelper.Set(ref flags, settings.GetFlag());
    }
    
    private void OnDone(StatusEffect statusEffect)
    {
        statusEffect.onDone -= OnDone;
        statusEffect.Settings.CreateEndFx(statusEffect.Target.transform);
    }

    private bool RefreshUniqueStatusEffect(AbstractStatusEffectSettings settings)
    {      
        StatusEffect effect = statusEffects
            .Find(e => e.Settings.GetType() == settings.GetType() && e.Settings.isUnique);

        effect?.Refresh(settings);

        return effect != null;
    }

    private bool IsApplyable(AbstractStatusEffectSettings settings)
    {
        return statusEffectMask.Contains(settings.GetFlag());
    }

    private void Update()
    {
        foreach (StatusEffect statusEffect in statusEffects) {
            statusEffect.Update(Time.deltaTime);
        }

        RemoveEffects();
    }

    private void RemoveEffects()
    {
        foreach (StatusEffect statusEffect in statusEffects.Where(e => e.IsFinished).ToArray()) {
            RemoveEffect(statusEffect);
        }
    }

    private void RemoveEffect(StatusEffect statusEffect)
    {
        if (statusEffect == null) {
            return;
        }
        
        statusEffects.Remove(statusEffect);
            
        if (!IsAnotherStatusEffectOfTheSameTypeApplied(statusEffect)) {
            FlagsHelper.Unset(ref flags, statusEffect.Settings.GetFlag());
        }
            
        statusEffect.End();
    }

    private bool IsAnotherStatusEffectOfTheSameTypeApplied(StatusEffect mine)
    {
        return statusEffects
            .Any(other =>
                other.Settings.GetFlag() == mine.Settings.GetFlag() &&
                !other.IsFinished);
    }
}