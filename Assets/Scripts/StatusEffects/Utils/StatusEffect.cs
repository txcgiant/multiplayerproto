﻿using System;
using UnityEngine;

[Serializable]
public class StatusEffect
{
    //DEBUG
    [SerializeField]
    private StatusEffectFlag flag;
    
    public Action<StatusEffect> onDone;
    public GameObject Target { get; private set; }
    public AbstractStatusEffectSettings Settings { get; private set; }
    public Boolean IsFinished => duration <= 0;

    private AbstractStatusEffectStrategy Strategy { get; }

    [SerializeField]
    private float duration;

    public StatusEffect(GameObject target, AbstractStatusEffectSettings settings, AbstractStatusEffectStrategy strategy)
    {
        flag = settings.GetFlag();
        
        Target = target;
        Settings = settings;
        Strategy = strategy;

        duration = settings.duration;
    }

    public void Start()
    {
        Strategy.OnStart();
    }
    
    public void Update(float delta)
    {
        if (IsFinished) {
            return;
        }
        
        duration -= delta;

        Strategy.OnTick(delta);
    }

    public void End()
    {
        Strategy.OnEnd();
        onDone?.Invoke(this);
        Target = null;
    }

    public void Refresh(AbstractStatusEffectSettings settings)
    {
        Strategy.OnEnd();
        
        Settings = settings;
        duration = settings.duration;
        
        Strategy.OnStart();
    }
}