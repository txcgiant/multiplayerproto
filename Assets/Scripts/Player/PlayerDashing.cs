﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerDashing : MonoBehaviour
{
    public float strenght = 20;
    public float cooldown = 0.5f;
    
    private CharacterController controller;
    private InputManager inputManager;

    public Action<bool> Dashing { get; set; }

    private bool canDash = true;
    
    private void OnEnable () {
        controller = GetComponent<CharacterController>();
        inputManager = InputManager.instance;
        canDash = true;
    }

    public void Update()
    {
        if (!controller.isGrounded) {
            return;
        }
        
        if (inputManager.DashKeyPressed) {
            StartCoroutine(Dash());
        }
    }
   
    private IEnumerator Dash()
    {
        if (!canDash || !controller.isGrounded) {
            yield break;
        }

        InvokeDashing(false);

        Vector3 dashDirection = new Vector3(inputManager.LeftStick.x, 0, inputManager.LeftStick.y) * 0.1f;
		
        for (int i = 0; i < strenght; i++) {
            yield return new WaitForSeconds(0.005f);
            controller.Move(dashDirection);
        }

        yield return new WaitForSeconds(cooldown);
        InvokeDashing(true);
    }

    private void InvokeDashing(bool dashed)
    {
        canDash = dashed;
        Dashing.Invoke(canDash);
    }
}
