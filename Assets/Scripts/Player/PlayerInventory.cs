﻿using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerInventory : MonoBehaviour
{   
    [SerializeField]
    private Collectable collected;

    public int GetCollectedViewId()
    {
        return GetCollected().photonView.viewID;
    }
    
    public Collectable GetCollected()
    {
        return collected;
    }

    public PhotonView GetCollectedView()
    {
        return IsEmpty() ? null : collected.photonView;
    }

    public void SetCollected(Collectable collectable)
    {
        collected = collectable;
    }
    
    public void ClearCollected()
    {
        collected = null;
    }

    public bool IsEmpty()
    {
        return collected == null;
    }

    public bool HasWaterblob()
    {
        return HasComponent<Waterblob>();
    }

    public bool HasCauldron()
    {
        return HasComponent<CauldronSmall>();
    }

    public bool HasPlant()
    {
        return HasComponent<Plant>();
    }

    public TComponent Get<TComponent>() where TComponent : Component
    {
        return collected != null ? collected.GetComponent<TComponent>() : null;
    }
    
    private bool HasComponent<TComponent>() where TComponent : Component
    {
        return collected != null && Get<TComponent>() != null;
    }
}