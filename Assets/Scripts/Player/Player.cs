﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerIdentifier))]
[RequireComponent(typeof(PlayerInteractable))]
[RequireComponent(typeof(PlayerInventory))]
[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerDashing))]
[RequireComponent(typeof(PlayerNetworking))]
[RequireComponent(typeof(PlayerRespawn))]
[RequireComponent(typeof(PlayerTrail))]
[RequireComponent(typeof(PlayerUi))]
[RequireComponent(typeof(PlayerAnimation))]
[RequireComponent(typeof(StatusEffectsHolder))]
[RequireComponent(typeof(PlayerAgent))]
[RequireComponent(typeof(PlayerShooting))]

public class Player : Photon.MonoBehaviour
{
    public CharacterController controller;
    public PlayerIdentifier identifier;
    public PlayerInteractable interacting;
    public PlayerInventory inventory;
    public PlayerMovement movement;
    public PlayerDashing dashing;
    public PlayerNetworking network;
    public PlayerRespawn respawning;
    public PlayerTrail trail;
    public PlayerUi userInterface;
    public PlayerAnimation animations;
    public StatusEffectsHolder statusEffectHolder;
    public PlayerAgent agent;
    public PlayerShooting shooting;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        identifier = GetComponent<PlayerIdentifier>();
        interacting = GetComponent<PlayerInteractable>();
        inventory = GetComponent<PlayerInventory>();
        movement = GetComponent<PlayerMovement>();
        dashing = GetComponent<PlayerDashing>();
        network = GetComponent<PlayerNetworking>();
        respawning = GetComponent<PlayerRespawn>();
        trail = GetComponent<PlayerTrail>();
        userInterface = GetComponent<PlayerUi>();
        animations = GetComponent<PlayerAnimation>();
        statusEffectHolder = GetComponent<StatusEffectsHolder>();
        agent = GetComponent<PlayerAgent>();
        shooting = GetComponent<PlayerShooting>();
    }

    public int GetPlayerViewId()
    {
        return photonView.viewID;
    }
}