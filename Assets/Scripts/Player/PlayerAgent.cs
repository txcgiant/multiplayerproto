﻿using UnityEngine;

public class PlayerAgent : Agent, IControlFearProvider
{
    private Player player;

    private new void Awake()
    {
        base.Awake();
        player = GetComponent<Player>();

        if (!photonView.isMine) {
            enabled = false;
        }
    }
    
    protected override void Update()
    {
        base.Update();
        
        if (enabled) {
            player.animations.SetMoveSpeed(Mathf.Abs(agent.velocity.x) + Mathf.Abs(agent.velocity.z));
        }
    }
    
    public override void StartAgent()
    {
        if (currentMovementStrategy == null || !photonView.isMine) {
            return;
        }

        agent.enabled = true;
        enabled = true;
        WarpToNavMesh();
        StartCurrentStrategy();
    }

    /**
     * Sets Strategy to null and Stops the Agent.
     */
    private void ChangeToDefaultStrategy()
    {
        StopAgent();
        ChangeStrategy(null);
    }

    public void StartFear(float radius)
    {
        if (!photonView.isMine) {
            return;
        }
        
        player.movement.enabled = false;
        player.controller.enabled = false;
        
        ChangeStrategy(new FearMovementStrategy(transform.position, radius));
        
        if (player.statusEffectHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN | StatusEffectFlag.FEAR)) {
            enabled = false;
        } else {
            StartAgent();
        }
    }

    public void StopFear()
    {
        if (!photonView.isMine) {
            return;
        }
        
        if (!player.statusEffectHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN | StatusEffectFlag.FEAR)) {
            player.movement.enabled = true;
            player.controller.enabled = true;
        }

        if (!player.statusEffectHolder.HasEffect(StatusEffectFlag.FEAR)) {
            ChangeToDefaultStrategy();
        }
    }
}