﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerIdentifier : Photon.MonoBehaviour
{
    public GameObject hat;

    private void Start()
    {
        if (photonView.owner.CustomProperties[PlayerCustomProperties.ColorIndex] == null) {
            return;
        }
        
        int colorIndex = (int)photonView.owner.CustomProperties[PlayerCustomProperties.ColorIndex];
        Material mat = GameManager.instance.settings.playerColorMaterials.materials[colorIndex];

        Material[] mats = hat.GetComponent<Renderer>().materials;
        mats[0] = mat;

        hat.GetComponent<Renderer>().materials = mats;
    }
}