using JetBrains.Annotations;
using UnityEngine;

public class PlayerNetworking : Photon.MonoBehaviour, IPunObservable 
{
	private PlayerTrail trail;

	private void OnEnable()
	{
		trail = GetComponent<PlayerTrail>();
	}

	[UsedImplicitly]
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext(trail.particles.emission.enabled);
			stream.SendNext(trail.particles.main.startSize.constant);
			
		} else {
			ParticleSystem.EmissionModule emission = trail.particles.emission;
			emission.enabled = (bool) stream.ReceiveNext();

			ParticleSystem.MainModule main = trail.particles.main;
			main.startSize = (float)stream.ReceiveNext();
		}
	}
}
