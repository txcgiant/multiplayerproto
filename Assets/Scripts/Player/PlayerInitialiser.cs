﻿using UnityEngine;

// TODO: Maybe remove that script soon and move the code in components
public class PlayerInitialiser : Photon.MonoBehaviour
{
    public void Start()
    {
        if (!photonView.isMine) {
            return;
        }

        ActivateCamera();
        EnableComponents();
    }

    private void ActivateCamera()
    {
        GameObject cam = GameObject.Find("Main Camera");
        cam.SetActive(true);
        cam.GetComponent<CameraFollow>().target = gameObject.transform;
    }

    private void EnableComponents()
    {
        GetComponent<CharacterController>().enabled = true;
        
        GetComponent<PlayerMovement>().enabled = true;
        GetComponent<PlayerInteractable>().enabled = true;
        
        GetComponent<PlayerIdentifier>().enabled = true;
        GetComponent<PlayerDashing>().enabled = true;
        
        GetComponent<PlayerTrail>().enabled = true;
        GetComponent<PlayerRespawn>().enabled = true;
    }
}