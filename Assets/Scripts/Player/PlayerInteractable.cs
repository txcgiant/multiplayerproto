﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerInteractable : Photon.MonoBehaviour, IControlSilenceProvider
{
    private List<Interactable> interactables;
    private Player player;
    
    [SerializeField]
    private Interactable target;
    private Interactable lastTarget;

    private void OnEnable()
    {
        if (!photonView.isMine) {
            return;
        }
        InputManager.instance.onUseKeyDown += OnUseKeyDown;
        InputManager.instance.onUseKeyUp += OnUseKeyUp;
        InputManager.instance.onDashKeyDown += OnAbort;
    }

    private void OnDisable()
    {
        if (!photonView.isMine) {
            return;
        }
        InputManager.instance.onUseKeyDown -= OnUseKeyDown;
        InputManager.instance.onUseKeyUp -= OnUseKeyUp;
        InputManager.instance.onDashKeyDown -= OnAbort;
    }

    private void Awake()
    {
        player = GetComponent<Player>();
    }

    public void Start()
    {
        interactables = new List<Interactable>();
    }

    private void Update()
    {
        if (photonView.isMine) {
            FindTarget();
        }
    }

    private void OnAbort()
    {
        if (target != null) {
            target.Abort(player);
        }
    }

    private void OnUseKeyDown()
    {
        lastTarget = target;
        
        if (!target && PlayerHasCollected()) {
            player.inventory.GetCollected().Drop(player);
            return;
        }

        if (target) {
            target.InteractStart(player);
        }
    }

    private bool PlayerHasCollected()
    {
        return player.inventory.enabled && !player.inventory.IsEmpty();
    }

    private void OnUseKeyUp()
    {
        if (target != lastTarget) {
            return;
        }
        
        if (!target && PlayerHasCollected()) {
            player.inventory.GetCollected().InteractStop(player);
            return;
        }

        if (target) {
            target.InteractStop(player);
        }
    }

    private void FindTarget()
    {
        List<Interactable> filteredList = interactables
            .Where(interactable => interactable.CanInteract(player))
            .ToList();

        if (filteredList.Count == 0) {
            SetTarget(null);
            return;
        }
            
        filteredList.Sort(CompareInteractables);

        SetTarget(filteredList.First());
    }

    private void SetTarget(Interactable newTarget)
    {
        if (target == newTarget) {
            return;
        }
        
        if (target != null) {
            target.IsNotTheTargetAnymore(player);
        }
        
        target = newTarget;
        
        if (target != null) {
            target.HasBecomeTheTarget(player);
        }
    }

    public void RemoveInteractableFromList(Interactable interactable)
    {
        if (!interactables.Contains(interactable)) {
            return;
        }

        interactables.Remove(interactable);

        if (interactable == target) {
            SetTarget(null);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Interactable[] otherInteractables = other.GetComponents<Interactable>();
        
        if (otherInteractables == null || otherInteractables.Length == 0) {
            return;
        }

        interactables.AddRange(otherInteractables);

        foreach (var otherInteractable in otherInteractables) {
            otherInteractable.GetPlayersInRange().Add(player);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Interactable[] otherInteractables = other.GetComponents<Interactable>();
        
        if (otherInteractables == null || otherInteractables.Length == 0) {
            return;
        }

        interactables = interactables.Except(otherInteractables).ToList();
        
        foreach (var otherInteractable in otherInteractables) {
            otherInteractable.GetPlayersInRange().Remove(player);
        }
    }

    private int CompareInteractables(Interactable a, Interactable b)
    {
        float priorityA = a.GetPriority();
        float priorityB = b.GetPriority();
        
        if (priorityA < priorityB) {
            return 1;
        }
        
        if (priorityA > priorityB) {
            return -1;
        }
        
        return CompareDistance(a, b);
    }
    
    private int CompareDistance(Component a, Component b)
    {
        float distanceToA = DistanceTo(a);
        float distanceToB = DistanceTo(b);
        
        if (distanceToA < distanceToB) {
            return -1;
        }
        
        if (distanceToA > distanceToB) {
            return 1;
        }
        
        return 0;
    }
    
    private float DistanceTo(Component obj)
    {
        return Vector3.Distance(transform.position, obj.transform.position);
    }
    
    private void Silence(bool silenced)
    {
        if (silenced && !target && PlayerHasCollected()) {
            player.inventory.GetCollected().Drop(player);
        }
        
        enabled = !silenced;
        player.inventory.enabled = !silenced;
    }

    public void OnSilenceStart()
    {
        Silence(true);
    }

    public void OnSilenceEnd()
    {
        Silence(false);
    }
}