﻿using System;
using UnityEngine.Events;

public class PlayerUi : Photon.MonoBehaviour
{
    public SaplingSelectionDisplay saplingSelection;
    public RootStatusEffectSettings rootEffect;

    private Player player;
    
    private void Start()
    {
        if (!photonView.isMine) {
            enabled = false;
            return;
        }
        
        player = GetComponent<Player>();
        saplingSelection = FindObjectOfType<SaplingSelectionDisplay>();
    }

    public void ShowSaplingSelection(Planting planting, UnityAction<PlantType> callback)
    {
        saplingSelection.onSelectionDone.AddListener(callback);
        saplingSelection.Show(planting);
        
        player.statusEffectHolder.Apply(rootEffect);
    }

    public void InteractWithSaplingSelection()
    {
        saplingSelection.Interact();
        HideSaplingSelection();
    }

    public void HideSaplingSelection()
    {
        saplingSelection.onSelectionDone.RemoveAllListeners();
        saplingSelection.Hide();
        player.statusEffectHolder.Remove(rootEffect);
    }
}