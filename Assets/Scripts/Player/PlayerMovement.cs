﻿using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerMovement : Photon.MonoBehaviour, IControlImpairingSpeedProvider, IControlRootProvider
{
	public Vector3 Velocity {
		set { velocity = value; }
	}

	[SerializeField]
	private float speed = 10f;
	[SerializeField]
	private float roationSpeed = 15f;
	[SerializeField]
	private float gravity = 20f;
	[SerializeField]
	private float snapDistance = 1.4f;

	private float speedMultiplier = 1f;

	private InputManager inputManager;
	private PlayerAnimation animations;
	private CharacterController controller;
	private Vector3 velocity = Vector3.zero;
	private Player player;

	private void Awake()
	{
		inputManager = InputManager.instance;
		
		player = GetComponent<Player>();
		controller = player.controller;
		animations = player.animations;
	}

	private void Update ()
	{
		if (controller.isGrounded) {
			Move();
		} else {
			Snap();
		}

		animations.SetGrounded(controller.isGrounded);

		velocity.y -= gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime);

		animations.SetMoveSpeed(Mathf.Abs(velocity.x) + Mathf.Abs(velocity.z));

		Rotate();
	}

	private void Snap()
	{
		RaycastHit hitInfo;
		if (Physics.Raycast(new Ray(transform.position, Vector3.down), out hitInfo, snapDistance)) {
			controller.Move(hitInfo.point - transform.position);
		}
	}

	private void Move()
	{
		velocity = new Vector3(inputManager.LeftStick.x, 0, inputManager.LeftStick.y) * speed * speedMultiplier;
	}

	private void Rotate()
	{
		Vector3 newDir = Vector3.RotateTowards(
			transform.forward,
			new Vector3(velocity.x, 0f, velocity.z),
			roationSpeed * Time.deltaTime,
			0.0f);

		transform.rotation = Quaternion.LookRotation(newDir);
	}

	public void SpeedUp(float multiplier)
	{
		speedMultiplier *= multiplier;
	}

	public void SlowDown(float multiplier)
	{
		speedMultiplier /= multiplier;
	}
	
	public void StartRoot()
	{
		Velocity = Vector3.zero;
		player.animations.SetMoveSpeed(0);
		
		if (!photonView.isMine) {
			return;
		}
		Root(true);
	}

	public void EndRoot()
	{
		if (!photonView.isMine) {
			return;
		}
		
		if (!player.statusEffectHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN)) {
			Root(false);
		}
	}
	
	private void Root(bool rooted)
	{
		if (rooted) {
			player.agent.StopAgent();
			enabled = false;
			player.dashing.enabled = false;
			
		} else if(player.statusEffectHolder.HasEffect(StatusEffectFlag.FEAR)) {
			player.agent.StartAgent();
		} else {
			enabled = true;
			player.dashing.enabled = true;
			controller.enabled = true;
		}
	}
}