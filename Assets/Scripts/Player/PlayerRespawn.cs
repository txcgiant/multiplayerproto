﻿using UnityEngine;

public class PlayerRespawn : Photon.MonoBehaviour
{ 
    public float minHeight = -10f;
    public GameObject spawnEffect;
    
    private GameObject[] spawnPoints;
    private Vector3 spawnPoint;

    private PlayerMovement movement;
    
    private void Start()
    {
        spawnPoints = GameManager.instance.spawnPoints;
        spawnPoint = Vector3.zero;

        movement = GetComponent<PlayerMovement>();
    }

    private void FixedUpdate()
    {
        if (transform.position.y >= minHeight) {
            return;
        }

        if (spawnPoints.Length > 0) {
            spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
        }
        
        movement.Velocity = Vector3.zero;
        transform.position = spawnPoint;
        spawnEffect.SetActive(true);
    }
}