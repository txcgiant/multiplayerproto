﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PlayerShooting : Photon.MonoBehaviour
{
    public GameObject prefab;
    public RootStatusEffectSettings rootEffect;
    
    public float rotationSpeed = 10;
    public float shootDelay;
    public Vector2 projectileSpawnForwardOffset = new Vector2(0.5f, 0.5f);
    
    private float lastShootTime;
    private int lastProjectileId;

    private readonly List<Projectile> projectiles = new List<Projectile>();

    private Player player;
    private InputManager input;

    private bool isAiming;

    private void OnEnable()
    {
        InputManager.instance.onShootKeyDown += ShootKeyDown;
        InputManager.instance.onShootKeyUp += ShootKeyUp;
    }

    private void OnDisable()
    {
        InputManager.instance.onShootKeyDown -= ShootKeyDown;
        InputManager.instance.onShootKeyUp += ShootKeyUp;
    }

    public void Start()
    {
        player = GetComponent<Player>();
        input = InputManager.instance;
    }
    
    public void SendProjectileHit(int projectileId)
    {
        if(PhotonNetwork.offlineMode) {
            RPC_OnProjectileHit(projectileId);
            
        } else {
            photonView.RPC("RPC_OnProjectileHit",
                PhotonTargets.Others, projectileId);
        }
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_OnProjectileHit(int projectileId)
    {
        projectiles.RemoveAll(item => item == null);

        Projectile projectile = projectiles.Find(item => item.Id == projectileId);

        if (projectile == null) {
            return;
        }

        projectile.OnProjectileHit();
        projectiles.Remove(projectile);
    }

    private void Update()
    {
        if (!isAiming) {
            return;
        }

        if (input.IsMouseMoving()) {

            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
         
            if (Physics.Raycast(cameraRay, out hit, 1000f, 1 << LayerMask.NameToLayer("Ground"))) {
                RotateTo(hit.point - transform.position);
            }
            
        } else if (input.IsLeftStickMoving()) {
            RotateTo(new Vector3(input.LeftStick.x, 0, input.LeftStick.y));
        }
    }

    private void RotateTo(Vector3 direction)
    {
        direction.y = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
    }

    private void ShootKeyUp()
    {
        lastProjectileId++;

        Shoot();
        player.statusEffectHolder.Remove(rootEffect);
        isAiming = false;
    }

    private void ShootKeyDown()
    {
        if (!photonView.isMine) {
            return;
        }
        
        player.statusEffectHolder.Apply(rootEffect);
        isAiming = true;
    }

    private void Shoot()
    {
        if (Time.realtimeSinceStartup - lastShootTime < shootDelay) {
            return;
        }

        if (PhotonNetwork.offlineMode) {
            
            RPC_PlayerShooting_OnShoot(
                GetProjectileSpawnPosition(),
                player.transform.rotation,
                lastProjectileId,
                new PhotonMessageInfo()
            );
            
        } else {
            photonView.RPC("RPC_PlayerShooting_OnShoot", PhotonTargets.All,
                GetProjectileSpawnPosition(),
                player.transform.rotation,
                lastProjectileId);
        }
    }

    [PunRPC]
    private void RPC_PlayerShooting_OnShoot(Vector3 position, Quaternion rotation, int projectileId, PhotonMessageInfo info)
    {
        double timestamp = PhotonNetwork.time;
    
        /*if (info.sender != null) {
            timestamp = info.timestamp;
        }*/
            
        CreateProjectile( position, rotation, timestamp, projectileId );
    }
    
    private void CreateProjectile(Vector3 position, Quaternion rotation, double createTime, int projectileId)
    {
        lastShootTime = Time.realtimeSinceStartup;

        GameObject newProjectileObject = Instantiate(prefab, new Vector3(0, -100, 0), rotation);
        newProjectileObject.name = "Zapp_" + newProjectileObject.name;

        Projectile newProjectile = newProjectileObject.GetComponent<Projectile>();

        newProjectile.SetCreationTime(createTime);
        newProjectile.SetStartPosition(position);
        newProjectile.SetProjectileId(projectileId);

        newProjectile.Owner = player;

        projectiles.Add(newProjectile);
    }
    
    private Vector3 GetProjectileSpawnPosition()
    {
        float calculate = PhotonNetwork.GetPing() * 0.001f;
        return new Vector3(
            transform.position.x + player.transform.forward.x * (projectileSpawnForwardOffset.x + calculate),
            transform.position.y + player.transform.up.y * (projectileSpawnForwardOffset.y + calculate),
            transform.position.z + player.transform.forward.z * (projectileSpawnForwardOffset.x + calculate));
    }
}