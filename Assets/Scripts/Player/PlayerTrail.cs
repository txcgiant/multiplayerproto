﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerTrail : MonoBehaviour
{
    public ParticleSystem particles;

    private CharacterController controller;
    private PlayerDashing dashing;
    
    private float startSize;

    public void OnEnable()
    {
        startSize = particles.main.startSize.constant;
        controller = GetComponent<CharacterController>();
        dashing = GetComponent<PlayerDashing>();
        
        dashing.Dashing += ActivedDash;
    }

    public void Update()
    {
        ParticleSystem.EmissionModule emission = particles.emission;
        emission.enabled = controller.isGrounded;
    }

    private void ActivedDash(bool canDash)
    {
        ParticleSystem.MainModule main = particles.main;
        main.startSize = !canDash ? startSize * 2f : startSize;
    }
}
