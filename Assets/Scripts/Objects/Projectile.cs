﻿using DG.Tweening;
using UnityEngine;

public class Projectile : Photon.MonoBehaviour, IPoolable
{
    [Header("Values")]
    public float startSpeed = 4;
    public float maxSpeed = 4;
    public float timeToMaxSpeed = 0.7f;
    public Ease ease;
    public float lifeTime = 4;
    
    [Header("Hit")]
    public GameObject hitFx;
    public AbstractStatusEffectSettings effect;

    public Player Owner { private get; set; }
    public int Id { get; private set; }

    private float speed = 1;
    private double creationTime;
    private Vector3 startPosition;

    private StatusEffectsHolder holder;

    private void Start()
    {
        speed = startSpeed;
        DOTween.To(()=> speed, x=> speed = x, maxSpeed, timeToMaxSpeed).SetEase(ease);
    }

    private void Update()
    {
        if (holder != null) {
            return;
        }
        
        float timePassed = (float)(PhotonNetwork.time - creationTime);
        transform.position = startPosition + transform.forward * speed * timePassed;

        if(timePassed > lifeTime) {
            Destroy(gameObject);
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Ground")) {
            OnProjectileHit();
            return;
        }

        holder = collision.collider.GetComponent<StatusEffectsHolder>();

        if (holder == null) {
            return;
        }

        if(!holder.photonView.isMine) {
            return;
        }

        OnProjectileHit();
        Owner.shooting.SendProjectileHit(Id);
    }

    public void SetCreationTime(double creationTime)
    {
        this.creationTime = creationTime;
    }

    public void SetStartPosition(Vector3 position)
    {
        startPosition = position;
    }

    public void SetProjectileId(int projectileId)
    {
        Id = projectileId;
    }
    
    public void OnProjectileHit()
    {
        Destroy(gameObject);
        CreateHitFx();

        if (holder != null) {
            holder.Apply(effect);
        }
    }
    
    private void CreateHitFx()
    {
        Instantiate(hitFx, transform.position, transform.rotation);
    }

    public void Reset()
    {
        Owner = null;
        Id = -1;
        speed = 1;
        creationTime = 0;
        startPosition = Vector3.zero;
    }
}