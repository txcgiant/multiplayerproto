﻿using System.Collections;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(WaterConsumer))]
public class GrowingAction : Photon.MonoBehaviour
{
    public PlantType plantType;
    public int numberSlippingPlants = 1;
    public float spawnRadius = 0.2f;
    
    public float growTime = 3;
    public float slipScale = 3;

    public UnityEvent onGrowStart;
    public UnityEvent onGrowEnd;

    private void Start()
    {
        GetComponent<WaterConsumer>().onWaterlimitReached.AddListener(StartGrowing);
    }

    private void StartGrowing()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }

        StartCoroutine(Grow());
    }
    
    private IEnumerator Grow()
    {
        photonView.RPC("RPC_GrowingAction_OnGrowStart", PhotonTargets.All);
        yield return new WaitForSeconds(growTime);
        photonView.RPC("RPC_GrowingAction_OnGrowEnd", PhotonTargets.All);

        if (PhotonNetwork.isMasterClient) {
            InstantiantePlants();
        }
    }

    [PunRPC]
    [UsedImplicitly]
    private void RPC_GrowingAction_OnGrowStart()
    {
        transform.DOScale(slipScale, growTime);
        onGrowStart?.Invoke();
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_GrowingAction_OnGrowEnd()
    {
        onGrowEnd?.Invoke();
    }

    private void InstantiantePlants()
    {
        if (numberSlippingPlants == 1) {
            SpawnPlant();
        } else {
            SpawnPlants();
        }

        PhotonNetwork.Destroy(photonView);
    }

    private void SpawnPlant()
    {
        PhotonNetwork.InstantiateSceneObject(
            GameManager.instance.plantMatcher.GetPlantPrefab(plantType).name,
            transform.position,
            Quaternion.identity, 0, null); 
    }

    private void SpawnPlants()
    {
        float angle = GetStartAngle(numberSlippingPlants);
        float range = GetDistanceBetweenSections(numberSlippingPlants);
        for (int i = 0; i < numberSlippingPlants; i++) {
            PhotonNetwork.InstantiateSceneObject(
                GameManager.instance.plantMatcher.GetPlantPrefab(plantType).name,
                GetSpawnpoint(angle),
                Quaternion.identity, 0, null);

            angle += range;
        }
    }

    private Vector3 GetSpawnpoint(float angle)
    {
        float x = spawnRadius * Mathf.Cos(angle);
        float z = spawnRadius * Mathf.Sin(angle);
        
        return new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
    }
    
    private float GetDistanceBetweenSections(int plantCount) => 2 * Mathf.PI / plantCount;
    private float GetStartAngle(int plantCount) => plantCount > 2 ? GetUpAngle(): 0;
    private float GetUpAngle() => -(3 * Mathf.PI / 2);
}