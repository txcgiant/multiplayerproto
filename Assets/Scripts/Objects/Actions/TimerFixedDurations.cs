﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

public class TimerFixedDurations : MonoBehaviour
{
    public List<float> waitDurations;

    public UnityEvent onIntervallFinished;

    private void Start()
    {
        StartCoroutine(Intervall());
    }

    private IEnumerator Intervall()
    {
        foreach (float waitDuration in waitDurations) {
            yield return new WaitForSeconds(waitDuration);
            onIntervallFinished?.Invoke();
        }
    }
}
    
    