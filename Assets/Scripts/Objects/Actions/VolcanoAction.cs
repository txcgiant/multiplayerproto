﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class VolcanoAction : Photon.MonoBehaviour, IOnParticleCollisionReceiver
{
    public FearStatusEffectSettings settings;
    public ParticleSystem partSystem;
    public float intervall;

    private IEnumerator eruptingCoroutine;
    private Collider[] hitColliders;
    private readonly List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    private StatusEffectsHolder statusEffectsHolder;
    private bool hitIgnitableObject;
    
    private void Awake()
    {
        eruptingCoroutine = EruptInFixedIntervall();
        StartErupting();
    }

    [UsedImplicitly]
    public void StartErupting()
    {
        StartCoroutine(eruptingCoroutine);
    }

    [UsedImplicitly]
    public void StopErupting()
    {
        StopCoroutine(eruptingCoroutine);
    }

    private IEnumerator EruptInFixedIntervall()
    {
        while (true) {
            yield return new WaitForSeconds(intervall);
            EruptIfMaster();
        }
    }

    public void OnParticleCollision(GameObject other)
    {
        partSystem.GetCollisionEvents(other, collisionEvents);
        
        hitColliders = Physics.OverlapSphere(collisionEvents[0].intersection, .2f);

        hitIgnitableObject = false;
        foreach (Collider hitCollider in hitColliders) {
            if (Ignite(hitCollider.gameObject)) {
                hitIgnitableObject = true;
            }
        }

        if (!hitIgnitableObject) {
            SpawnBurningGroundIfMaster(collisionEvents[0].intersection);
        }
    }

    private bool Ignite(GameObject gameObject)
    {
        statusEffectsHolder = gameObject.GetComponent<StatusEffectsHolder>();

        if (statusEffectsHolder != null && statusEffectsHolder.photonView.isMine) {
            statusEffectsHolder.Apply(settings);
            return true;
        }

        return false;
    }
    
    private void SpawnBurningGroundIfMaster(Vector3 position)
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        PhotonNetwork.InstantiateSceneObject(
            "Burning Ground", 
            position,
            Quaternion.identity, 
            0, null);
    }

    private void EruptIfMaster()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }

        photonView.RPC(
            "RPC_VolcanoAction_Erupt",
            PhotonTargets.All,
            transform.position,
            transform.rotation,
            (uint)Random.Range(uint.MinValue, uint.MaxValue)
        );
    }

    [PunRPC]
    [UsedImplicitly]
    private void RPC_VolcanoAction_Erupt(Vector3 position, Quaternion rotation, uint seed)
    {
        //partSystem.randomSeed = seed;
        partSystem.transform.position = position;
        partSystem.transform.rotation = Quaternion.Euler(rotation.eulerAngles.x - 90, rotation.eulerAngles.y, rotation.eulerAngles.z);
        partSystem.Play();
    }
}
