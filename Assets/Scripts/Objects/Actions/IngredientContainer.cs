﻿    using System.Collections.Generic;
    using JetBrains.Annotations;
    using UnityEngine;
    using UnityEngine.Events;

public class IngredientContainer : MonoBehaviour
{
    public int capacity = 3;

    public IngredientUnityEvent onIngredientAdded;
    public IngredientsUnityEvent onIngredientsChanged;
    public UnityEvent onIngredientsCleared;
    public UnityEvent onCapacityReached;

    [SerializeField]
    protected List<Ingredient> ingredients;

    private void Awake()
    {
        ingredients = new List<Ingredient>(capacity);
    }

    public int GetIngredientCount()
    {
        return ingredients.Count;
    }

    [UsedImplicitly]
    public void Clear()
    {
        ingredients.Clear();
        onIngredientsCleared?.Invoke();
        onIngredientsChanged?.Invoke(ingredients);
    }

    public bool CanAddIngredients()
    {
        return ingredients.Count < capacity;
    }

    public void AddIngredient(Ingredient ingredient)
    {
        ingredients.Add(ingredient);
        onIngredientAdded?.Invoke(ingredient);
        onIngredientsChanged?.Invoke(ingredients);

        if (!CanAddIngredients()) {
            onCapacityReached.Invoke();
        }
    }

    public RecipeResult GetResult()
    {
        return GameManager.instance.recipeBook.GetRecipe(ingredients);
    }

    public MasterResult CheckMasterRecipe(MasterRecipe recipe)
    {
        return recipe.GetResult(ingredients);
    }
}