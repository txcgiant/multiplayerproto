﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class HoveringAction : Photon.MonoBehaviour
{
    public Vector3 offset = new Vector3(0, 1f, 0);
    private Transform target;
    private Vector3 hoverPosition;

    public DropMotion dropMotion;
    
    [Header("Hovering")]
    public float hoverPositionLerp = 0.05f;
    public float hoverToPlaceLerp = 0.25f;
    public float hoverRotationLerp = 0.04f;
    
    public float hoverRange = 0.1f;
    public float hoverSpeed = 3f;
    public float hoverRotationFactor = 4f;

    private float currentPositionLerp;

    public UnityEvent onHoverToPlayer;
    public UnityEvent onHoverAtPosition;
    public UnityEvent onHoverToGround;
    
    public UnityEvent onHoverToGroundFinished;
    // TODO: Event for has reached drop position
    // TODO: Event for has first reached the highest point

    private bool isHovering;
    
    [UsedImplicitly]
    public void HoverOverPlayer(Player player)
    {
        target = player.transform;
        currentPositionLerp = hoverPositionLerp;
        onHoverToPlayer?.Invoke();
        isHovering = true;
    }

    public void HoverAtPosition(Vector3 position)
    {
        target = null;
        isHovering = true;
        hoverPosition = position;
        currentPositionLerp = hoverToPlaceLerp;
        onHoverAtPosition?.Invoke();
    }

    public void StopHovering()
    {
        transform.rotation = Quaternion.identity;
        isHovering = false;
    }

    [UsedImplicitly]
    public void HoverToGround()
    {
        Vector3 dropPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        
        RaycastHit hitInfo;
        Vector3 down = transform.TransformDirection(Vector3.down);
        // TODO Create a scriptable object so that others than programmer can change these values
        if (Physics.Raycast(new Ray(dropPosition + transform.up * 5, down), out hitInfo, 10f, 1 << LayerMask.NameToLayer("Ground"))) {
            dropPosition = hitInfo.point;
        }

        photonView.RPC("RPC_HoverToGround", PhotonTargets.All, dropPosition);
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_HoverToGround(Vector3 position)
    {
        if (!isHovering) {
            return;
        }
        
        onHoverToGround?.Invoke();
        StopHovering();

        dropMotion.Play(transform, position, () =>
        {
            target = null;
            onHoverToGroundFinished?.Invoke();
        });
    }
    
    public void LateUpdate()
    {
        if (!isHovering) {
            return;
        }
        
        if (target != null) {
            hoverPosition = GetPositionWithOffset();
        }
        
        UpdateTilt();

        float hoverY = Mathf.Sin(Time.time * hoverSpeed) * hoverRange;
        
        transform.localPosition = 
            Vector3.Lerp(
                transform.localPosition,
                hoverPosition + Vector3.up * hoverY,
                currentPositionLerp);
    }

    private void UpdateTilt()
    {
        Vector3 velocity = hoverPosition - transform.localPosition;
        
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            Quaternion.Euler(new Vector3(-velocity.z, velocity.y, velocity.x) * hoverRotationFactor),
            hoverRotationLerp);
    }

    private Vector3 GetPositionWithOffset()
    {
        return new Vector3(
            target.position.x + offset.x * target.forward.x,
            target.position.y + offset.y,
            target.position.z + offset.z * target.forward.z);
    }
}