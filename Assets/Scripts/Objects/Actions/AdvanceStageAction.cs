﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(IngredientContainer))]
public class AdvanceStageAction : MonoBehaviour
{
    private IngredientContainer ingredientContainer;
    private int stage;
   
    public List<MasterRecipe> masterRecipes;
	
    public MasterResultUnityEvent onStageCompleted;
    public MasterRecipeUnityEvent onNewRecipe;
    public IntUnityEvent onNewStage;

    private void Start()
    {
        ingredientContainer = GetComponent<IngredientContainer>();
        ingredientContainer.onIngredientAdded.AddListener(CheckForStageComplete);
        onNewRecipe?.Invoke(masterRecipes[stage]);
    }

    private void CheckForStageComplete(Ingredient ingredient = null)
    {
        if (!HasMoreStages()) {
            return;
        }
              
        if (IsStageCompleted()) {
            AdvanceStage();
        }
    }

    public void AdvanceStage()
    {
        MasterResult result = masterRecipes[stage].result;
        onStageCompleted?.Invoke(result);
        stage++;
        ingredientContainer.Clear();
        onNewStage?.Invoke(stage);
        
        if (HasNextStage()) {
            onNewRecipe?.Invoke(masterRecipes[stage]);
        } else {
            GameManager.instance.GameOver();
        }
    }

    private bool HasMoreStages()
    {
        return stage < masterRecipes.Count;
    }

    private bool IsStageCompleted()
    {
        return ingredientContainer.CheckMasterRecipe(masterRecipes[stage]) != null;
    }

    public bool HasNextStage()
    {
        return stage < masterRecipes.Count;
    }
}

