﻿using System.Collections;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(WaterSupplier))]
public class Waterblob : Photon.MonoBehaviour, IPunObservable
{
    private WaterSupplier waterSupplier;
    private int waterSpend;
    
    public WaterblobSettings settings;
    
    public UnityEvent onDestroy;
    public FloatUnityEvent onWaterChanged;

    private void Awake()
    {
        waterSupplier = GetComponent<WaterSupplier>();
        waterSupplier.onWatering.AddListener(SpendWater);
    }

    private void Start()
    {
        StartCoroutine(LoseWaterOverTime());
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        onWaterChanged?.Invoke(PercentLeft());
    }

    private IEnumerator LoseWaterOverTime()
    {
        while (HasWater()) {
            yield return new WaitForSeconds(1);
            SpendWater(settings.waterLossPerSecond);
        }
    }

    private void SpendWater(int amount)
    {
        waterSpend += amount;

        onWaterChanged?.Invoke(PercentLeft());
        
        if (!HasWater() && PhotonNetwork.isMasterClient) {
            PhotonNetwork.Destroy(photonView);
        }
    }

    private bool HasWater()
    {
        return waterSpend < settings.waterCapacity;
    }
    
    private float PercentLeft()
    {
        return 1 - (float)waterSpend / settings.waterCapacity;
    }

    [UsedImplicitly]
    public void Dry(float duration)
    {
        DOTween.To(GetWaterSpend, SpendWater, settings.waterCapacity, duration);
    }

    private int GetWaterSpend()
    {
        return waterSpend;
    }
    
    [UsedImplicitly]
    public void Destroy()
    {       
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        waterSupplier.onWatering.RemoveListener(SpendWater);
        
        onDestroy?.Invoke();
    }
}