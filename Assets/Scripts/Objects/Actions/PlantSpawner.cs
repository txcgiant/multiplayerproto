﻿using JetBrains.Annotations;
using UnityEngine;

public class PlantSpawner : MonoBehaviour
{
    public PlantType plantType;
    public Vector3 offset;
    
    [UsedImplicitly]
    public void SpawnPlant()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        PhotonNetwork.InstantiateSceneObject(
            GameManager.instance.plantMatcher.GetPlantPrefab(plantType).name,
            transform.position + offset,
            Quaternion.identity, 0, null);
    }
}