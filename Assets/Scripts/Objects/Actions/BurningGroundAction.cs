﻿
    using System.Collections;
    using JetBrains.Annotations;
    using UnityEngine;

[RequireComponent(typeof(WaterConsumer))]
public class BurningGroundAction : Photon.MonoBehaviour
{
    public FearStatusEffectSettings settings;
    
    [Tooltip("WaterConsumer gets Water each Second to simulate burning down of the Fire")]
    public int waterPerSecond = 10;
    
    private WaterConsumer waterConsumer;
    
    private void Awake()
    {
        waterConsumer = GetComponent<WaterConsumer>();
    }

    private void Start()
    {
        StartCoroutine(DecreaseFireIntensity());
    }

    private void OnCollisionEnter(Collision other)
    {             
        StatusEffectsHolder statusEffectsHolder = other.collider.GetComponent<StatusEffectsHolder>();

        if (statusEffectsHolder != null) {
            statusEffectsHolder.Apply(settings);
        }
    }
    
    private IEnumerator DecreaseFireIntensity()
    {
        while (!waterConsumer.EnoughtWater()) {
            yield return new WaitForSeconds(1);
            waterConsumer.IncreaseWater(waterPerSecond);
        }
    }

    [UsedImplicitly]
    public void StartDestroyIfMaster()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }

        StartCoroutine(DestroyInOneSecond());
    }

    private IEnumerator DestroyInOneSecond()
    {
        yield return new WaitForSeconds(0.2f);
        PhotonNetwork.Destroy(gameObject);
    }
}
