﻿using JetBrains.Annotations;
using UnityEngine;

public class HoverPlaceAction : Photon.MonoBehaviour
{
    public Vector3 offset = new Vector3(0, 1, 0);
    
    [UsedImplicitly]
    public void StartHovering(Player player)
    {
        Collectable collectable = player.inventory.GetCollected();
        
        if (collectable == null) {
            return;
        }
        
        collectable
            .GetComponent<HoveringAction>()
            .HoverAtPosition(transform.position + offset);
    }
    
    [UsedImplicitly]
    public void StopHovering(Player player)
    {
        Collectable collectable = player.inventory.GetCollected();
        
        if (collectable == null) {
            return;
        }
        
        collectable
            .GetComponent<HoveringAction>()
            .HoverOverPlayer(player);
    }
}