﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class TimerFixedIntervall : MonoBehaviour
{
    public float intervallLength;

    public UnityEvent onIntervallFinished;

    private void Start()
    {
        StartCoroutine(Intervall());
    }

    private IEnumerator Intervall()
    {
        while (true) {
            yield return new WaitForSeconds(intervallLength);
            onIntervallFinished?.Invoke();
        }
    }
}