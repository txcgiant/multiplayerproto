﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class WaterSupplier : MonoBehaviour
{
    public WateringSettings settings;
    
    public UnityEvent onWateringStarted;
    public IntUnityEvent onWatering;
    public UnityEvent onWateringEnded;

    private const float TickLength = 0.025f;
    
    private Coroutine wateringCoroutine;
    
    public void StartWatering(WaterConsumer waterConsumer)
    {
        onWateringStarted?.Invoke();
        
        wateringCoroutine = StartCoroutine(IncreaseWater(waterConsumer));
    }

    public void StopWatering()
    {
        onWateringEnded?.Invoke();
        
        StopCoroutine(wateringCoroutine);
    }
    
    private IEnumerator IncreaseWater(WaterConsumer waterConsumer)
    {
        while (!waterConsumer.EnoughtWater()) {
            yield return new WaitForSeconds(TickLength);   
            waterConsumer.IncreaseWater(settings.wateringAmountPerTick);
            onWatering?.Invoke(settings.wateringAmountPerTick);
        }
    }
}