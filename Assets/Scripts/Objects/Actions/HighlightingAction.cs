﻿using Boo.Lang;
using JetBrains.Annotations;
using UnityEngine;

public class HighlightingAction : MonoBehaviour
{
    private const string HighlightPropertyName = "_Highlighted";
    private List<Material> materials;
    
    private void OnEnable()
    {
        materials = new List<Material>();
        Renderer thisRenderer = GetComponent<Renderer>();
        
        if (thisRenderer != null) {
            GetAllHighlightableMaterialsFromRenderer(thisRenderer);
            return;
        }

        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

        foreach (Renderer r in renderers) {
            foreach (Material material in r.materials) {
                if (material.HasProperty(HighlightPropertyName)) {
                    materials.Add(material);
                }
            }
        }
    }

    private void GetAllHighlightableMaterialsFromRenderer(Renderer r)
    {
        foreach (Material material in r.materials) {
            if (material.HasProperty(HighlightPropertyName)) {
                materials.Add(material);
            }
        }
    }

    [UsedImplicitly]
    public void Highlight(bool highlight)
    {
        foreach (Material material in materials) {
            material.SetFloat(HighlightPropertyName, highlight ? 1f : 0f); 
        }
    }
}