﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractableRpcReceiver))]
[RequireComponent(typeof(PhotonView))]
public abstract class Interactable : Photon.MonoBehaviour
{
    [Header("Highlighting Events")]
    public PlayerUnityEvent hasBecomeTheTarget;
    public PlayerUnityEvent isNotTheTargetAnymore;
    
    [Header("Highlighting Events Synced")]
    public PlayerUnityEvent hasBecomeTheTargetSynced;
    public PlayerUnityEvent isNotTheTargetAnymoreSynced;
    
    protected bool isInteracting;
    protected float currentInteractTime;
    
    private readonly List<Player> playersInRange = new List<Player>();
    [SerializeField]
    private int priority;
    [SerializeField]
    private int interactableId = -1;

    protected virtual void Awake()
    {
        if (!HasValidId()) {
            AssignInteractableId();
        }
    }

    private void AssignInteractableId()
    {
        Interactable[] interactables = GetComponents<Interactable>();

        for (int i = 0; i < interactables.Length; i++) {
            interactables[i].interactableId = i;
        }
    }

    private bool HasValidId()
    {
        return interactableId > 0;
    }
    
    protected virtual void ExecuteStart(Player player) {}
    protected virtual void Execute(Player player) {}
    protected virtual void ExecuteStop(Player player) {}
    protected virtual void ExecuteAbort(Player player) {}

    public void Abort(Player player)
    {   
        isInteracting = false;
        currentInteractTime = 0;
        ExecuteAbort(player);
    }

    public void InteractStart(Player player)
    {
        isInteracting = true;

        ExecuteStart(player);
        Execute(player);
    }
        
    public void InteractStop(Player player)
    {
        if (!isInteracting) {
            return;
        }
        
        ExecuteStop(player);
        isInteracting = false;
        currentInteractTime = 0;
    }
    
    public void HasBecomeTheTarget(Player player)
    {
        hasBecomeTheTarget?.Invoke(player);

        if (hasBecomeTheTargetSynced != null) {
            photonView.RPC(
                "RPC_InteractableNetworking_HasBecomeTheTarget",
                PhotonTargets.All,
                player.photonView.viewID,
                interactableId
            );
        }
    }

    public void HasBecomeTheTargetSynced(int playerId)
    {
        hasBecomeTheTargetSynced?.Invoke(PhotonUtils.GetComponentByViewId<Player>(playerId));
    }

    public void IsNotTheTargetAnymore(Player player)
    {
        if (isInteracting) {
            InteractStop(player);
        }
        
        isNotTheTargetAnymore?.Invoke(player);

        if (isNotTheTargetAnymoreSynced != null) {
            photonView.RPC(
                "RPC_InteractableNetworking_IsNotTheTargetAnymore",
                PhotonTargets.All,
                player.photonView.viewID,
                interactableId
            );
        }
    }

    public void IsNotTheTargetAnymoreSynced(int playerId)
    {
        isNotTheTargetAnymoreSynced?.Invoke(PhotonUtils.GetComponentByViewId<Player>(playerId));
    }

    protected virtual void Update()
    {
        if (!isInteracting) {
            return;
        }
        
        currentInteractTime += Time.deltaTime;
    }

    public int GetPriority()
    {
        return priority;
    }

    public virtual bool CanInteract(Player player)
    {
        return true;
    }

    private void OnDestroy()
    {
        RemoveInteractableFromList();
    }

    protected void RemoveInteractableFromList()
    {
        foreach (Player player in playersInRange) {
            if (player != null) {
                player.interacting.RemoveInteractableFromList(this);
            }
        }
    }

    public List<Player> GetPlayersInRange()
    {
        return playersInRange;
    }
}