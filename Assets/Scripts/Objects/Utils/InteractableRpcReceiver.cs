﻿using JetBrains.Annotations;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class InteractableRpcReceiver : MonoBehaviour
{
    private Interactable[] interactables;

    private void Awake()
    {
        interactables = GetComponents<Interactable>();
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_InteractableNetworking_HasBecomeTheTarget(int playerId, int id)
    {
        interactables[id].HasBecomeTheTargetSynced(playerId);
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_InteractableNetworking_IsNotTheTargetAnymore(int playerId, int id)
    {
        interactables[id].IsNotTheTargetAnymoreSynced(playerId);
    }
}