﻿
using UnityEngine;

[RequireComponent(typeof(StatusEffectsHolder))]
[RequireComponent(typeof(PlantAgent))]
public class Plant : Photon.MonoBehaviour
{
    public Ingredient ingredient;
    public PlantType plantType;
    
    public StatusEffectsHolder statusEffectsHolder;
    public PlantAgent agent;

    private void Awake()
    {
        statusEffectsHolder = GetComponent<StatusEffectsHolder>();
        agent = GetComponent<PlantAgent>();
    }
}