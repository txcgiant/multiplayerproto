﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(Plant))]
public class PlantAgent : Agent, IControlFearProvider, IControlRootProvider
{
    public MovementStrategyMonoBehaviour defaultMovementStrategy;
    private Plant plant;

    protected override void Awake()
    {
        base.Awake();
        plant = GetComponent<Plant>();
    }

    private void Start()
    {
        ChangeToDefaultStrategy();
        StartAgent();
    }

    /**
     * Changes the current MovementStrategy to the default Movement Strategy and starts the Agent if it is not allready running.
     */
    private void ChangeToDefaultStrategy()
    {        
        ChangeStrategy(defaultMovementStrategy);
    }   
    
    [UsedImplicitly]
    public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        if (IsRunning()) {
            StartAgent();
        }
    }

    public void StartFear(float radius)
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        if(plant.statusEffectsHolder.HasEffect(StatusEffectFlag.FEAR)) {
            return;
        }
        
        ChangeStrategy(new FearMovementStrategy(transform.position, radius));
        
        if (plant.statusEffectsHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN)) {
            StopAgent();
        } else {
            StartAgent();
        }
    }

    public void StopFear()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        if (!plant.statusEffectsHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN | StatusEffectFlag.FEAR)) {
            StartAgent();
        }

        if (!plant.statusEffectsHolder.HasEffect(StatusEffectFlag.FEAR)) {
            ChangeToDefaultStrategy();
        }
    }

    public void StartRoot()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        StopAgent();
    }

    public void EndRoot()
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        if (!plant.statusEffectsHolder.HasEffect(StatusEffectFlag.ROOT | StatusEffectFlag.STUN)) {
            StartAgent();
        }
    }
}