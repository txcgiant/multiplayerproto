﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(IngredientContainer))]
public class IngredientContainerEditor : Editor
{
    public Ingredient ingredient;
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ingredient = (Ingredient)EditorGUILayout.ObjectField("Ing to Add", ingredient, typeof(Ingredient), true);
        if (GUILayout.Button("Add Ingredient")) {
            ((IngredientContainer) target).AddIngredient(ingredient);
        }
    }
}