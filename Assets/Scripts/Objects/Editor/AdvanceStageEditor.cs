﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AdvanceStageAction))]
public class AdvanceStageEditor : Editor {
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        if (GUILayout.Button("Advance Stage")) {
            if (((AdvanceStageAction) target).HasNextStage()) {
                ((AdvanceStageAction) target).AdvanceStage();
            }
        }
    }
}