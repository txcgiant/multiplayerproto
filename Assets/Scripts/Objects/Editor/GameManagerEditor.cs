﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public PlantType plantType;
    public Vector3 spawnPosition = Vector3.zero;
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        plantType = (PlantType)EditorGUILayout.EnumPopup("Plant to create:", plantType);
        spawnPosition = EditorGUILayout.Vector3Field("Spawn position:", spawnPosition);
        if (GUILayout.Button("Spawn Plant")) {
            PhotonNetwork.InstantiateSceneObject(
                GameManager.instance.plantMatcher.GetPlantPrefab(plantType).name,
                spawnPosition, 
                Quaternion.identity, 0, null); 
        }
    }
}