﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;

public class Fireplace : Interactable
{
    public Vector3 offset = new Vector3(0, 0.15f, 0);
    public DropMotion drop;
    
    [SerializeField]
    private CauldronSmall cauldronSmall;

    protected override void Execute(Player playerInteractable)
    {
        photonView.RPC(
            "RPC_StopHoveringOverFireplace",
            PhotonTargets.All,
            playerInteractable.photonView.viewID,
            transform.position + offset
        );
    }

    public void ClearCauldronReference()
    {
        cauldronSmall = null;
    }

    public override bool CanInteract(Player player)
    {
        return cauldronSmall == null && player.inventory.HasCauldron();
    }
    
    [PunRPC]
    [UsedImplicitly]
    public void RPC_StopHoveringOverFireplace(int playerId, Vector3 position)
    {
        Player player = PhotonUtils.GetComponentByViewId<Player>(playerId);
        
        Collectable collectable = player.inventory.GetCollected();
        collectable.GetComponent<HoveringAction>().StopHovering();
        
        cauldronSmall = collectable.GetComponent<CauldronSmall>();
        cauldronSmall.SetFireplace(this);
        collectable.ResetCollectable(player);

        drop.Play(collectable.transform, position, null);
    }
}