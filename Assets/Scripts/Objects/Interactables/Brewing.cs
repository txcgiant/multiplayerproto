﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collectable))]
[RequireComponent(typeof(CauldronSmall))]
[RequireComponent(typeof(IngredientContainer))]
public class Brewing : Interactable, IPunObservable
{
    private readonly BrewState[] states = {
        new IdleState(), new BrewingState(), new StoppedState(), new CompletedState()
    };

    private const byte Idle = 0;
    private const byte Brew = 1;
    private const byte Stopped = 2;
    private const byte Completed = 3;
    
    public float collectLeeway = 0.2f;

    private Collectable collectable;
    private CauldronSmall cauldron;
    private IngredientContainer ingredientContainer;
    private BrewState state;

    [SerializeField]
    private RecipeResult recipeResult;
    private float brewDuration = 1f;

    private float currentBrewTime;

    private int brewerId;

    public SpriteUnityEvent onRecipeFound;
    public FloatUnityEvent onBrewTimeChanged;
    public UnityEvent onBrewStart;
    public UnityEvent onBrewPaused;
    public UnityEvent onBrewContinued;
    public RecipeResultUnityEvent onBrewComplete;
    public UnityEvent onNormalBrewComplete;
    public FloatUnityEvent onPerfectBrewComplete;
    public UnityEvent onBrewReset;

    protected override void Awake()
    {
        base.Awake();
        collectable = GetComponent<Collectable>();
        cauldron = GetComponent<CauldronSmall>();
        ingredientContainer = GetComponent<IngredientContainer>();
        ingredientContainer.onCapacityReached.AddListener(FindResult);
        
        
        SetState(states[Idle]);
    }

    private void SetState(BrewState newState)
    {
        state?.OnStateExit();
        state = newState;
        state.SetController(this);
        state.OnStateEnter();
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        float oldBrewTime = currentBrewTime;
        stream.Serialize(ref currentBrewTime);

        if (oldBrewTime != currentBrewTime) {
            onBrewTimeChanged?.Invoke(PercentCompleted());
        }
    }

    private void SetCurrentBrewTime(float newTime)
    {
        currentBrewTime = newTime;
        onBrewTimeChanged?.Invoke(PercentCompleted());
    }

    private void IncreaseCurrentBrewtime(float increaseTime)
    {
        SetCurrentBrewTime(currentBrewTime + increaseTime);
    }

    protected override void ExecuteStop(Player player)
    {
        if (currentInteractTime <= collectLeeway) {
            collectable.InteractStart(player);
        }
        
        state.OnInteractionStops();
    }

    public override bool CanInteract(Player player)
    {
        return cauldron.IsOnFireplace() &&
               player.inventory.IsEmpty() &&
               ingredientContainer.GetIngredientCount() == ingredientContainer.capacity &&
               CanBrew();
    }

    private bool CanBrew()
    {
        return (PhotonNetwork.player.ID == brewerId || brewerId == 0);
    }

    protected override void Update()
    {
        base.Update();

        if (!isInteracting && state != states[1]) {
            return;
        }

        state.Update();
    }
    
    public void ResetBrewing()
    {
        onBrewReset?.Invoke();
        
        //ToDo: hier brauchen wir glaub keine RPC, da ResetBrewing aktuell bereits auf allen aufgerufen werden sollte -JK
        photonView.RPC("RPC_ChangeBrewState", PhotonTargets.All, Idle);
    }

    private bool BrewingStartet()
    {
        return currentBrewTime > 0f;
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_ChangeBrewState(byte stateId)
    {
        ChangeBrewState(stateId, 0);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_StartBrewing(int brewerId)
    {
        ChangeBrewState(Brew, brewerId);
    }

    private void ChangeBrewState(byte stateId, int brewerId)
    {
        BrewState newState = states[stateId];

        if (newState.Equals(state)) {
            return;
        }
        
        SetState(newState);
        this.brewerId = brewerId;
    }

    private void FindResult()
    {
        recipeResult = ingredientContainer.GetResult();
        brewDuration = recipeResult.brewDuration;
        onRecipeFound.Invoke(recipeResult.ingredient.image);
    }
    
    private float PercentCompleted()
    {
        return currentBrewTime / brewDuration;
    }
    
    // ##############
    // ### States ###
    // ##############
    
    private class IdleState : BrewState
    {
        public override void Update()
        {
            if (controller.currentInteractTime > controller.collectLeeway) {
                controller.photonView.RPC("RPC_StartBrewing", PhotonTargets.All, PhotonNetwork.player.ID);
            }
        }
    }
    
    private class BrewingState : BrewState
    {
        public override void OnStateEnter()
        {
            if (controller.BrewingStartet()) {
                controller.onBrewContinued?.Invoke();
            } else {
                controller.onBrewStart?.Invoke();
            }
        }

        public override void OnInteractionStops()
        {
            controller.photonView.RPC("RPC_ChangeBrewState", PhotonTargets.All, Stopped);
        }

        public override void Update()
        {
            if (controller.currentBrewTime >= controller.brewDuration && PhotonNetwork.isMasterClient) {
                controller.photonView.RPC("RPC_ChangeBrewState", PhotonTargets.All, Completed);
            }
            
            controller.IncreaseCurrentBrewtime(Time.deltaTime);
        }
    }
    
    private class StoppedState : BrewState
    {
        public override void OnStateEnter()
        {
            controller.onBrewPaused?.Invoke();
        }

        public override void Update()
        {
            controller.photonView.RPC("RPC_StartBrewing", PhotonTargets.All, PhotonNetwork.player.ID);
        }
    }
    
    private class CompletedState : BrewState
    {
        public override void OnStateEnter()
        {
            controller.onBrewComplete?.Invoke(controller.recipeResult);
            controller.cauldron.SetIngredient(controller.recipeResult.ingredient);
            controller.SetCurrentBrewTime(controller.brewDuration);

            if (controller.recipeResult.isPerfect) {
                GameManager.instance.IncreaseLevelTime(controller.recipeResult.timeBonusIfPerfect);
                controller.onPerfectBrewComplete?.Invoke(controller.recipeResult.timeBonusIfPerfect);
            } else {
                controller.onNormalBrewComplete?.Invoke();
            }
        }
        
        public override void OnStateExit()
        {
            controller.SetCurrentBrewTime(0f);
            controller.ingredientContainer.Clear();
            controller.recipeResult = null;
        }
    }

    private abstract class BrewState
    {
        protected Brewing controller;
    
        public virtual void Update() {}
        public virtual void OnInteractionStops() {}
        public virtual void OnStateEnter() {}
        public virtual void OnStateExit() {}

        public void SetController(Brewing newController)
        {
            controller = newController;
        }
    }
}