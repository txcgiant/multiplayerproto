﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class Planting : Interactable
{
    public List<PlantType> possiblePlants;
    public Vector3 plantOffset = new Vector3(0f, 0.13f, 0f);

    [Header("Growing")]
    public UnityEvent onPlant;
    public UnityEvent onSlip;
    
    [Header("Water Consumer")]
    public WaterPlaceUnityEvent onWaterConsumerAdded;
    public WaterPlaceUnityEvent onWaterConsumerRemoved;
    
    private GameObject sapling;
    private int selectedPlantTyleIndex;

    private bool isSaplingSelectionShown;

    private void Start()
    {
        isNotTheTargetAnymore.AddListener(HideSaplingSelection);
    }
    
    protected override void Execute(Player player)
    {
        PlayerUi playerUi = player.userInterface;
        
        if (!isSaplingSelectionShown) {
            playerUi.ShowSaplingSelection(this, Plant);
            isSaplingSelectionShown = true;
        } else {
            playerUi.InteractWithSaplingSelection();
        }
    }

    protected override void ExecuteAbort(Player player)
    {
        HideSaplingSelection(player);
    }

    private void HideSaplingSelection(Player player)
    {
        player.userInterface.HideSaplingSelection();
        isSaplingSelectionShown = false;
    }
    
    private void Plant(PlantType type)
    {
        isSaplingSelectionShown = false;
        
        photonView.RPC("RPC_InstantiateViaMaster", PhotonTargets.MasterClient,
            GameManager.instance.plantMatcher.GetSaplingPrefab(type).name,
            transform.position + plantOffset,
            Quaternion.Euler(0, Random.Range(-120, -160), 0));
    }

    [PunRPC]
    [UsedImplicitly]
    private void RPC_InstantiateViaMaster(string path, Vector3 position, Quaternion rotation)
    {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }
        
        GameObject obj = 
            PhotonNetwork.InstantiateSceneObject(path, position, rotation, 0, null);
        
        photonView.RPC(
            "RPC_Plant",
            PhotonTargets.All,
            obj.GetPhotonView().viewID
        );
    }
    
    [PunRPC]
    [UsedImplicitly]
    public void RPC_Plant(int id)
    {
        Plant(PhotonView.Find(id).gameObject);
        sapling.transform.parent = transform;
        
        sapling.GetComponent<GrowingAction>()
            .onGrowEnd.AddListener(RemoveSaplingPlanted);
    }
    
    public override bool CanInteract(Player player)
    {
        return !IsSaplingPlanted() && player.inventory.IsEmpty();
    }

    private void RemoveSaplingPlanted()
    {
        sapling.GetComponent<GrowingAction>()
            .onGrowEnd.RemoveListener(RemoveSaplingPlanted);
        
        onSlip?.Invoke();
        
        WaterConsumer waterConsumer = sapling.GetComponent<WaterConsumer>();
        if (waterConsumer != null) {
            onWaterConsumerRemoved?.Invoke(waterConsumer);
        }

        ClearBed();
    }

    public bool IsSaplingPlanted()
    {
        return sapling != null;
    }

    public void Plant(GameObject sapling)
    {
        this.sapling = sapling;

        onPlant?.Invoke();
        
        WaterConsumer waterConsumer = sapling.GetComponent<WaterConsumer>();
        if (waterConsumer != null) {
            onWaterConsumerAdded?.Invoke(waterConsumer);
        }
        
    }

    public void ClearBed()
    {
        sapling = null;
    }
}