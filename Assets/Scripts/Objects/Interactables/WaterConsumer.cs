﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class WaterConsumer : Interactable, IPunObservable
{
    //ToDo: Scriptable Object
    public int waterNeeded = 200;

    public UnityEvent onWaterlimitReached;
    public FloatUnityEvent onWaterAmountChanged;

    [SerializeField]
    private int currentWater;

    public override bool CanInteract(Player player)
    {
        return player.inventory.HasWaterblob() && !EnoughtWater();
    }

    protected override void ExecuteStart(Player player)
    {
        photonView.RPC(
            "RPC_WaterConsumer_StartWatering", 
            PhotonTargets.AllViaServer, 
            player.photonView.viewID);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_WaterConsumer_StartWatering(int player)
    {
        Player playerInteractable =
            PhotonUtils.GetComponentByViewId<Player>(player);

        if (playerInteractable == null) {
            return;
        }
        
        WaterSupplier waterSupplier = playerInteractable.inventory.GetCollected().GetComponent<WaterSupplier>();
        waterSupplier.StartWatering(this);
    }
    
    protected override void ExecuteStop(Player player)
    {
        photonView.RPC(
            "RPC_WaterConsumer_StopWatering", 
            PhotonTargets.AllViaServer, 
            player.photonView.viewID);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_WaterConsumer_StopWatering(int playerId)
    {
        Player player =
            PhotonUtils.GetComponentByViewId<Player>(playerId);
        
        if (player == null || player.inventory.GetCollected()== null) {
            return;
        }
        
        WaterSupplier waterSupplier = player.inventory.GetCollected().GetComponent<WaterSupplier>();
        waterSupplier.StopWatering();
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        int oldWaterValue = currentWater;
        stream.Serialize(ref currentWater);

        if (oldWaterValue != currentWater) {
            onWaterAmountChanged?.Invoke(PercentFilled());
        }
    }

    public void IncreaseWater(int amount)
    {       
        AdjustCurrentWaterlevel(currentWater + amount);
    }

    private void AdjustCurrentWaterlevel(int amount)
    {
        currentWater = amount;
        onWaterAmountChanged?.Invoke(PercentFilled());
        
        if (EnoughtWater()) {
            onWaterlimitReached?.Invoke();
        }
    }

    public bool EnoughtWater()
    {
        return currentWater >= waterNeeded;
    }

    private float PercentFilled()
    {
        return (float)currentWater / waterNeeded;
    }
}