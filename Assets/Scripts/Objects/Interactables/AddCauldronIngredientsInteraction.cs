﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(IngredientContainer))]
public class AddCauldronIngredientsInteraction : Interactable
{
    private IngredientContainer container;

    protected override void Awake()
    {
        base.Awake();
        container = GetComponent<IngredientContainer>();
    }

    protected override void Execute(Player player)
    {
        photonView.RPC(
            "RPC_FillWithCauldronResult",
            PhotonTargets.All,
            player.inventory.GetCollectedViewId()
        );
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_FillWithCauldronResult(int cauldronId)
    {
        CauldronSmall cauldron = PhotonUtils.GetComponentByViewId<CauldronSmall>(cauldronId);

        container.AddIngredient(cauldron.GetIngredient());
        cauldron.ResetCauldron();
    }

    public override bool CanInteract(Player player)
    {
        return player.inventory.HasCauldron() && 
               container.CanAddIngredients() && 
               player.inventory.Get<CauldronSmall>().HasIngredient();
    }
}