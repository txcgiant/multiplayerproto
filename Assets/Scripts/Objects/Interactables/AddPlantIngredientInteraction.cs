﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(IngredientContainer))]
public class AddPlantIngredientInteraction : Interactable
{
    private IngredientContainer container;

    protected override void Awake()
    {
        base.Awake();
        container = GetComponent<IngredientContainer>();
    }

    protected override void Execute(Player player)
    {
        photonView.RPC(
            "RPC_FillWithPlant",
            PhotonTargets.All,
            player.inventory.GetCollectedViewId()
        );
    }

    [PunRPC]
    [UsedImplicitly]
    public void RPC_FillWithPlant(int plantId)
    {
        Plant plant = PhotonUtils.GetComponentByViewId<Plant>(plantId);

        container.AddIngredient(plant.ingredient);
    
        // TODO: Maybe fire an event

        if (PhotonNetwork.isMasterClient) {
            PhotonNetwork.Destroy(plant.photonView);
        }
    }
    
    public override bool CanInteract(Player player)
    {
        return player.inventory.HasPlant() && container.CanAddIngredients();
    }
}