﻿using JetBrains.Annotations;
using UnityEngine;

public class CollectableProducer : Interactable
{
	public GameObject prefab;

	public Vector3 offset;
	
	protected override void Execute(Player player)
	{
		photonView.RPC("RPC_AddCollectableToPlayer", PhotonTargets.All, player.photonView.viewID);
	}
	
	[PunRPC]
	[UsedImplicitly]
	public void RPC_AddCollectableToPlayer(int playerId)
	{
		if (!PhotonNetwork.isMasterClient) {
			return;
		}
		
		Collectable collectable = PhotonNetwork.InstantiateSceneObject(
			prefab.name,
			transform.position + offset,
			Quaternion.identity,
			0, null).GetComponent<Collectable>();
		
		Player player =
			PhotonUtils.GetComponentByViewId<Player>(playerId);

		collectable.PickUp(player);
	}
	
	public override bool CanInteract(Player player)
	{
		return player.inventory.IsEmpty();
	}
}
