﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class RemoveBuryedPlantInteraction : Interactable
{
    public bool IsBuryed { private get; set; }

    public UnityEvent onChannelStarted;
    public UnityEvent onChannelCompleted;
    public FloatUnityEvent onChannelTimeChanged;

    [SerializeField]
    private float timeNeeded = 2.5f;
    [SerializeField]
    private float timeChanneled;
    [SerializeField]
    private int numberChannelingPlayers = 0;


    
    public override bool CanInteract(Player player)
    {
        return player.inventory.IsEmpty() && IsBuryed;
    }
    
    protected override void Update()
    {
        base.Update();

        if (numberChannelingPlayers == 0) {
            return;
        }
        
        IncreaseTimeChanneled(Time.deltaTime * numberChannelingPlayers);

        if (ChannelCompleted() && PhotonNetwork.isMasterClient) {
            photonView.RPC(
                "RPC_ChannelInteractable_ChannelCompleted", 
                PhotonTargets.AllViaServer);
        }
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_ChannelInteractable_ChannelCompleted()
    {
        onChannelCompleted?.Invoke();
        timeChanneled = 0;
    }

    protected override void ExecuteStart(Player player)
    {
        photonView.RPC(
            "RPC_ChannelInteractable_StartChanneling", 
            PhotonTargets.AllViaServer); 
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_ChannelInteractable_StartChanneling()
    {
        numberChannelingPlayers++;
        onChannelStarted?.Invoke();
    }
    
    protected override void ExecuteStop(Player player)
    {
        if (!isInteracting) {
            return;
        } 
       
       photonView.RPC(
            "RPC_ChannelInteractable_StopChanneling", 
            PhotonTargets.AllViaServer);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_ChannelInteractable_StopChanneling()
    {
        numberChannelingPlayers--;
    }
    
    [UsedImplicitly]
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        float oldTime = timeChanneled;
        stream.Serialize(ref timeChanneled);

        if (oldTime != timeChanneled) {
            onChannelTimeChanged?.Invoke(PercentChanneled());
        }
    }

    private void IncreaseTimeChanneled(float time)
    {
        timeChanneled += time;

        if (timeChanneled > timeNeeded) {
            timeChanneled = timeNeeded;
        }
        
        onChannelTimeChanged?.Invoke(PercentChanneled());
    }

    private bool ChannelCompleted()
    {
        return timeChanneled >= timeNeeded;
    }

    private float PercentChanneled()
    {
        return timeChanneled / timeNeeded;
    }
}