﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(HoveringAction))]
public class Collectable : Interactable
{
    
    [Header("Collectable Events")]
    public PlayerUnityEvent onPickUp;
    public PlayerUnityEvent onDrop;

    private Player collectedBy;
    
    protected override void ExecuteStart(Player player)
    {
        Execute(player);
    }

    protected override void Execute(Player player)
    {
        PickUp(player);
    }

    public override bool CanInteract(Player player)
    {
        return HasFreeHands(player) && !IsAlreadyCollected();
    }

    public void PickUp(Player playerInteractable)
    {
        photonView.RPC("RPC_PickUp", PhotonTargets.AllViaServer, playerInteractable.photonView.viewID);
    }

    public void Drop(Player playerInteractable)
    {
        photonView.RPC("RPC_Drop", PhotonTargets.AllViaServer, playerInteractable.photonView.viewID);
    }

    public void ResetCollectable(Player player)
    {
        collectedBy = null;
        player.inventory.ClearCollected();
    }

    [PunRPC]
    [UsedImplicitly]
    private void RPC_PickUp(int playerId)
    {
        if (collectedBy != null) {
            return;
        }
        
        Player player =
            PhotonUtils.GetComponentByViewId<Player>(playerId);

        if (player == null) {
            return;
        }

        collectedBy = player;
        onPickUp?.Invoke(player);
        
        player.interacting.RemoveInteractableFromList(this);
        player.inventory.SetCollected(this);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_Drop(int playerId)
    {
        Player player =
            PhotonUtils.GetComponentByViewId<Player>(playerId);

        if (player == null) {
            return;
        }

        ResetCollectable(player);
        onDrop?.Invoke(player);
    }
    
    private bool HasFreeHands(Player player)
    {
        return player.inventory.IsEmpty();
    }

    private bool IsAlreadyCollected()
    {
        return collectedBy != null;
    }

    private void OnDestroy()
    {
        RemoveInteractableFromList();
        
        if (collectedBy != null) {
            collectedBy.inventory.ClearCollected();
        }
    }
}