﻿using System.Collections.Generic;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class DropPlantIntoAbyssInteraction : Interactable
{
    public List<PlantType> plantTypes;
    
    public override bool CanInteract(Player player)
    {
        return player.inventory.HasPlant() && plantTypes.Contains(player.inventory.GetCollected().GetComponent<Plant>().plantType);
    }
    
    protected override void ExecuteStart(Player player)
    {
        photonView.RPC(
            "RPC_ThrowPlantIntoAbyssInteraction_Drop", 
            PhotonTargets.AllViaServer, 
            player.inventory.GetCollectedViewId(),
            player.GetPlayerViewId()); 
        
    }

    [PunRPC]
    [UsedImplicitly]
    protected void RPC_ThrowPlantIntoAbyssInteraction_Drop(int collectableId, int playerId)
    {
        Collectable collectable =
            PhotonUtils.GetComponentByViewId<Collectable>(collectableId);
        
        Player player =
            PhotonUtils.GetComponentByViewId<Player>(playerId);

        if (collectable == null || player == null) {
            return;
        }

        collectable.ResetCollectable(player);
        collectable.GetComponent<HoveringAction>().StopHovering();
        
        collectable.transform.DOMoveY(-10, .5f).SetEase(Ease.InSine).OnComplete(()  => DestroyIfMaster(collectable.gameObject));
    }

    private void DestroyIfMaster(GameObject gameObject)
    {
        if (PhotonNetwork.isMasterClient) {
            PhotonNetwork.Destroy(gameObject);
        }
    }
}