﻿using UnityEngine;

[RequireComponent(typeof(Collectable))]
[RequireComponent(typeof(Brewing))]
public class CauldronSmall : Photon.MonoBehaviour
{
    [SerializeField]
    private Fireplace fireplace;
    private Ingredient ingredient;

    private Brewing brewing;

    private void Awake()
    {
        brewing = GetComponent<Brewing>();
        GetComponent<Collectable>().onPickUp.AddListener(ClearFireplace);
    }

    private void ClearFireplace(Player player = null)
    {
        if (fireplace == null) {
            return;
        }

        fireplace.ClearCauldronReference();
        fireplace = null;
    }

    public bool IsOnFireplace()
    {
        return fireplace != null;
    }

    public void SetFireplace(Fireplace fireplace)
    {
        this.fireplace = fireplace;
    }

    public Ingredient GetIngredient()
    {
        return ingredient;
    }

    public bool HasIngredient()
    {
        return ingredient != null;
    }

    public void SetIngredient(Ingredient ingredient)
    {
        this.ingredient = ingredient;
    }

    public void ResetCauldron()
    {
        brewing.ResetBrewing();
        SetIngredient(null);
    }
}