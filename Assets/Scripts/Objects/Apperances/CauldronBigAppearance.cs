﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

public class CauldronBigAppearance : MonoBehaviour
{
    private Material soupMaterial;
    private readonly List<List<ParticleSystem>> particleSystems = new List<List<ParticleSystem>>();
    private readonly List<List<ParticleSystem>> bubbles = new List<List<ParticleSystem>>();

    public Light cauldronLight;

    private void Start()
    {
        soupMaterial = GetComponent<Renderer>().materials[1];

        particleSystems.Add(FindFx(0));
        particleSystems.Add(FindFx(1));
        particleSystems.Add(FindFx(2));
        particleSystems.Add(FindFx(3));
        bubbles.Add(FindBubble(0));
        bubbles.Add(FindBubble(1));
        bubbles.Add(FindBubble(2));
    }

    private List<ParticleSystem> FindFx(int stage)
    {
        Transform stageContainer = transform.Find("FX Stage " + stage);
        List<ParticleSystem> fx = stageContainer.GetComponentsInChildren<ParticleSystem>().ToList();

        ParticleSystem parentParticleSystem = stageContainer.GetComponent<ParticleSystem>();
        if (parentParticleSystem != null) {
            fx.Add(parentParticleSystem);
        }

        return fx;
    }

    private List<ParticleSystem> FindBubble(int stage)
    {
        Transform stageContainer = transform.Find("Bubble Stage " + stage);
        List<ParticleSystem> fx = stageContainer.GetComponentsInChildren<ParticleSystem>().ToList();

        ParticleSystem parentParticleSystem = stageContainer.GetComponent<ParticleSystem>();
        if (parentParticleSystem != null) {
            fx.Add(parentParticleSystem);
        }

        return fx;
    }

    [UsedImplicitly]
    public void SwitchSoup(MasterResult result)
    {
        soupMaterial.color = result.albedo;
        soupMaterial.SetColor("_EmissionColor", result.emissionColor);
        soupMaterial.SetFloat("_Glossiness", result.smoothness);
    }

    [UsedImplicitly]
    public void SwitchLightColor(MasterResult result)
    {
        cauldronLight.color = result.lightColor;
        cauldronLight.intensity = result.lightIntensity;
    }

    [UsedImplicitly]
    public void SwitchFx(int stage)
    {
        switch (stage) {
            case 1:
                StopParticleSystems(particleSystems[0]);
                PlayParticleSystems(particleSystems[1]);
                DisableParticleSystems(bubbles[0]);
                EnableParticleSystems(bubbles[1]);
                break;
            case 2:
                StopParticleSystems(particleSystems[1]);
                PlayParticleSystems(particleSystems[2]);
                DisableParticleSystems(bubbles[1]);
                EnableParticleSystems(bubbles[2]);
                break;
            case 3:
                StopParticleSystems(particleSystems[2]);
                PlayParticleSystems(particleSystems[3]);
                DisableParticleSystems(bubbles[2]);
                break;
        }
    }

    private void StopParticleSystems(List<ParticleSystem> particleSystems)
    {
        foreach (ParticleSystem ps in particleSystems) {
            ps.Stop();
        }
    }

    private void PlayParticleSystems(List<ParticleSystem> particleSystems)
    {
        foreach (ParticleSystem ps in particleSystems) {
            ps.Play();
        }
    }

    private void DisableParticleSystems(List<ParticleSystem> particleSystems)
    {
        foreach (ParticleSystem ps in particleSystems) {
            ps.gameObject.SetActive(false);
        }
    }

    private void EnableParticleSystems(List<ParticleSystem> particleSystems)
    {
        foreach (ParticleSystem ps in particleSystems) {
            ps.gameObject.SetActive(true);
        }
    }
}
