﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class CauldronSmallAppearance : MonoBehaviour
{
    private Color defaultSoupColor;
    private Color defaultDustcloudEmissionColor;
    private Material fillMaterial;
    private Material durstcloudMaterial;

    public ParticleSystem splash;
    public ParticleSystem dustcloud;
    
    private void Awake()
    {
        fillMaterial = GetComponent<Renderer>().materials[1];
        defaultSoupColor = fillMaterial.color;

        durstcloudMaterial = dustcloud.GetComponent<Renderer>().materials[0];
        defaultDustcloudEmissionColor = durstcloudMaterial.GetColor("_EmissionColor");
    }

    [UsedImplicitly]
    public void ResetApperance()
    {
        fillMaterial.color = defaultSoupColor;

        SetDustcloudMaterialColor(defaultDustcloudEmissionColor);
    }

    [UsedImplicitly]
    public void SetApperanceFromRecipe(RecipeResult recipeResult)
    {
        fillMaterial.color = recipeResult.color;

        SetDustcloudMaterialColor(recipeResult.smokeEmissionColor);
    }

    private void SetDustcloudMaterialColor(Color color)
    {
        durstcloudMaterial.SetColor("_EmissionColor", color);
    }

    [UsedImplicitly]
    public void Splash()
    {
        splash.Play();
    }
}