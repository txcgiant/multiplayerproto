﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(Planting))]
public class BedAppearance : MonoBehaviour
{
    public BedAppearanceSettings settings;

    private Material soilMaterial;

    private void Awake()
    {       
        soilMaterial = GetComponent<Renderer>().materials[1];
    }

    private void Start()
    {
        ResetSoilStatus();
    }

    [UsedImplicitly]
    public void AddListenerSetWaterAmount(WaterConsumer waterConsumer)
    {
        waterConsumer.onWaterAmountChanged.AddListener(SetWaterAmount);
    }
    
    [UsedImplicitly]
    public void RemoveListenerSetWaterAmount(WaterConsumer waterConsumer)
    {
        waterConsumer.onWaterAmountChanged.AddListener(SetWaterAmount);
    }

    private void SetWaterAmount(float percentage)
    {
        soilMaterial.SetColor("Color_DD0E0962", Color.Lerp(settings.dryColor, settings.wetColor, percentage));
    }

    [UsedImplicitly]
    public void ResetSoilStatus()
    {
        soilMaterial.SetColor("Color_DD0E0962", settings.dryColor);
    }
}
