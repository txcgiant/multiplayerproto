﻿using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HetzkrautAppearance : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    [UsedImplicitly]
    public void EnableRunAnimation()
    {
        animator.SetBool("pickedUp", false);
    }

    [UsedImplicitly]
    public void DisableRunAnimation()
    {
        animator.SetBool("pickedUp", true);
    }
    
}
