﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;

namespace Objects.Apperances
{
    public class WaterblobAppearance  : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem rings;
        [SerializeField]
        private ParticleSystem droplets;

        public GameObject destroyedPrefab;

        public WaterblobAppearanceSettings settings;

        private void Start()
        {
            ParticleSystem.EmissionModule emissionModule = droplets.emission;
            emissionModule.rateOverDistance = new ParticleSystem.MinMaxCurve(
                settings.emissionOverDistanceMin, 
                settings.emissionOverDistanceMax
            );
        }

        [UsedImplicitly]
        public void StartWatering()
        {
            ParticleSystem.EmissionModule emissionModule = droplets.emission;
            emissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(
                settings.wateringEmissionOverTimeMin, 
                settings.wateringEmissionOverTimeMax
                );
        }

        [UsedImplicitly]
        public void StopWatering()
        {
            ParticleSystem.EmissionModule emissionModule = droplets.emission;
            emissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(
                settings.normalEmissionOverTimeMin, 
                settings.normalEmissionOverTimeMax);
        }

        [UsedImplicitly]
        public void SetScale(float scale)
        {
            transform.localScale = Vector3.one * scale;
            rings.transform.localScale = Vector3.one * scale;

            ParticleSystem.ShapeModule shapeModule = droplets.shape;
            shapeModule.radius = settings.dropletsRadius * scale;
        }

        [UsedImplicitly]
        public void CreateDestroyFx()
        {
            Instantiate(destroyedPrefab, transform).GetComponent<ParticleSystem>().Play();
        }
    }
}