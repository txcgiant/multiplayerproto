﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public abstract class LocationOfInterest : MonoBehaviour {
    public List<PlantType> viablePlantTypes;
    public int numberAgents;
   
    private readonly List<GameObject> nearbyPlayers = new List<GameObject>();
    private Savety savety = Savety.SAVE;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            nearbyPlayers.Add(other.gameObject);
        }

        UpdateSavety();
    }

    private void OnTriggerExit(Collider other)
    {
        if (nearbyPlayers.Contains(other.gameObject)) {
            nearbyPlayers.Remove(other.gameObject);
        }

        UpdateSavety();
    }
    
    private void UpdateSavety()
    {
        switch (nearbyPlayers.Count) {
            case 0:
                savety = Savety.SAVE;
                break;
            case 1:
                savety = Savety.RISKY;
                break;
            default:
                savety = Savety.DEADLY;
                break;
        }
    }
    
    public bool IsForPlantType(PlantType plantType)
    {
        return viablePlantTypes.Contains(plantType);
    }
    
    public int GetPriority()
    {
        return (int) savety + numberAgents;
    }
}

internal enum Savety{SAVE = 0, RISKY = 1, DEADLY = 2}
