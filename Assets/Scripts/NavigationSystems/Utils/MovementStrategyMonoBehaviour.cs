﻿using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public abstract class MovementStrategyMonoBehaviour : Photon.MonoBehaviour, IMovementStrategy
{
    public abstract void OnDestinationReached(Agent agent);

    public abstract void OnStart(Agent agent);

    public abstract void OnStop(Agent agent);

    public abstract float GetMinDistanceToDestination();
}