﻿using System.Runtime.InteropServices;

public interface IMovementStrategy
{
    void OnDestinationReached(Agent agent);
    void OnStart(Agent agent);
    void OnStop(Agent agent);
    float GetMinDistanceToDestination();
}