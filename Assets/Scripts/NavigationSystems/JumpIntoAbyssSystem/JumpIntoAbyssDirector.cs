﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class JumpIntoAbyssDirector : MonoBehaviour
{
	public static JumpIntoAbyssDirector instance;
	
	private readonly List<JumpIntoAbyssLocation> jumpLocations = new List<JumpIntoAbyssLocation>();

	private void Awake()
	{
		instance = this;
	}

	public void RegisterSuicideLocation(JumpIntoAbyssLocation jumpIntoAbyssLocation)
	{
		jumpLocations.Add(jumpIntoAbyssLocation);
	}

	public JumpIntoAbyssLocation GetAttractiveJumpLocation(PlantType plantType)
	{
		SortJumpLocations();

		List<JumpIntoAbyssLocation> lowestPriorityLocations = GetJumpLocationsWithLowestPriority(plantType);
		
		return lowestPriorityLocations[Random.Range(0, lowestPriorityLocations.Count - 1)];
	}

	private List<JumpIntoAbyssLocation> GetJumpLocationsWithLowestPriority(PlantType plantType)
	{
		SortJumpLocations();
		int lowestViablePriority = jumpLocations.Find(a => a.IsForPlantType(plantType)).GetPriority();
		return jumpLocations.FindAll(a => a.GetPriority() == lowestViablePriority && a.IsForPlantType(plantType));
	}

	private void SortJumpLocations()
	{
		jumpLocations
			.Sort((a,b) => a.GetPriority().CompareTo(b.GetPriority()));
	}
}