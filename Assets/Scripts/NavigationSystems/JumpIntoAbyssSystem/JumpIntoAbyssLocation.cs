﻿using UnityEngine;

[ExecuteInEditMode]
public class JumpIntoAbyssLocation : LocationOfInterest
{
    private GameObject jumpPoint;
    private GameObject landingPoint;

    private void Awake()
    {
        jumpPoint = transform.GetChild(0).gameObject;
        landingPoint = transform.GetChild(1).gameObject;

        JumpIntoAbyssDirector.instance.RegisterSuicideLocation(this);
    }

    public Vector3 GetJumpPosition()
    {
        return jumpPoint.transform.position;
    }

    public Vector3 GetLandingPosition()
    {
        return landingPoint.transform.position;
    }
}