﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(Agent))]
[RequireComponent(typeof(Collectable))]
public class JumpIntoAbyssStrategy : MovementStrategyMonoBehaviour
{   
    public PlantType plantType;
    public UnityEvent onJumpStarted;
    
    private const float MinDistanceToDestination = 0.5f;
    
    private Sequence jumpSequence;
    private JumpIntoAbyssLocation jumpLocation;
    private Collectable collectable;

    private void Start()
    {
        collectable = GetComponent<Collectable>();
    }
    
    public override void OnDestinationReached(Agent agent)
    {
        Suicide(agent);
    }
    
    private void Suicide(Agent agent)
    {
        StartSuicideSequence();
        collectable.onPickUp.AddListener(StopSuicideSequence);
        onJumpStarted?.Invoke();
        agent.StopAgent();
    }

    public override void OnStart(Agent agent)
    {
        SetNewSuicideLocation(agent);
    }

    private void SetNewSuicideLocation(Agent agent)
    {
        jumpLocation = JumpIntoAbyssDirector.instance.GetAttractiveJumpLocation(plantType);
        jumpLocation.numberAgents++;
        agent.SetDestination(jumpLocation.GetJumpPosition());
    }
    
    public override void OnStop(Agent agent)
    {
        if (jumpLocation == null) {
            return;
        }

        jumpLocation.numberAgents--;
        jumpLocation = null;
    }

    private void DestroyIfMaster()
    {
        if (PhotonNetwork.isMasterClient) {
           PhotonNetwork.Destroy(photonView); 
        }
    }

    private void StopSuicideSequence(Player player = null)
    {
        collectable.onPickUp.RemoveListener(StopSuicideSequence);
        jumpSequence?.Kill();
    }

    private void StartSuicideSequence()
    {
        jumpSequence = transform.DOJump(
            jumpLocation.GetLandingPosition(), 
            3, 
            1, 
            1.5f).OnComplete(DestroyIfMaster);
    }
    
    public override float GetMinDistanceToDestination()
    {
        return MinDistanceToDestination;
    }
}