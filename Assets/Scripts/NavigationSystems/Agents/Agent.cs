﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PhotonView))]
public abstract class Agent : Photon.MonoBehaviour, IControlImpairingSpeedProvider
{   
    protected NavMeshAgent agent;
    protected IMovementStrategy currentMovementStrategy;

    public Action onStrategyChanged;
    
    protected virtual void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = false;
    }
    
    public virtual void StartAgent()
    {
        if (currentMovementStrategy == null || !PhotonNetwork.isMasterClient && PhotonNetwork.connected) {
            return;
        }

        agent.enabled = true;
        enabled = true;
        WarpToNavMesh();
        StartCurrentStrategy();
    }
    
    protected void StartCurrentStrategy()
    {
        currentMovementStrategy?.OnStart(this);
    }

    protected virtual void Update()
    {
        if (IsRunning() && NearDestination()) {
            currentMovementStrategy.OnDestinationReached(this);
        }
    }
    
    public void StopAgent()
    {
        agent.enabled = false;
        enabled = false;
        StopCurrentStrategy();
    }

    private void StopCurrentStrategy()
    {
        currentMovementStrategy?.OnStop(this);
    }

    /**
     * Changes the current MovementStrategy and starts the Agent if it is not allready running.
     */
    public void ChangeStrategy(IMovementStrategy movementStrategy)
    {
        if (IsRunning()) {
            StopCurrentStrategy();
        }
        
        onStrategyChanged?.Invoke();
        currentMovementStrategy = movementStrategy;
        
        if (IsRunning()) {
            StartCurrentStrategy();
        }
    }

    public void SetDestination(Vector3 destination)
    {
        if (agent.enabled) {
            agent.destination = destination;
        }
    }
    
    private bool NearDestination()
    {
        return agent.hasPath && agent.remainingDistance < currentMovementStrategy.GetMinDistanceToDestination();
    }

    protected void WarpToNavMesh()
    {
        NavMeshHit navMeshHit;
        NavMesh.SamplePosition(transform.position, out navMeshHit, 500f, NavMesh.AllAreas);
        agent.Warp(navMeshHit.position);
    }

    protected bool IsRunning()
    {
        return agent.enabled;
    }
    
    public void SpeedUp(float multiplier)
    {
        agent.speed *= multiplier;
    }

    public void SlowDown(float multiplier)
    {
        agent.speed /= multiplier;
    }
}