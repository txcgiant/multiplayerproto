﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Agent))]
[RequireComponent(typeof(RemoveBuryedPlantInteraction))]
public class BlockBedsStrategy : MovementStrategyMonoBehaviour
{
    public MovementStrategyMonoBehaviour backupStrategy;
    
    private const float MinDistanceToDestination = 0.05f;

    private Agent agent;
    private Planting bed;
    private RemoveBuryedPlantInteraction removeBuryedPlantInteraction;
    private bool inBed;
    
    private void Start()
    {     
        removeBuryedPlantInteraction = GetComponent<RemoveBuryedPlantInteraction>();
    }

    public override void OnDestinationReached(Agent agent)
    {
        BlockBed();
    }
    
    public override void OnStart(Agent agent)
    {
        this.agent = agent;
        SetNewBed();
    }

    public override void OnStop(Agent agent)
    {
        if (bed != null) {
            bed.onPlant.RemoveListener(SetNewBed);
        }
    }
    
    private void SetNewBed()
    {
        if (inBed) {
            return;
        }
        
        if (bed != null) {
            bed.onPlant.RemoveListener(SetNewBed);
        }
        
        bed = BlockBedsDirector.instance.GetAttractiveBed(bed);
        
        if (bed == null) {
            agent.ChangeStrategy(backupStrategy);
            return;
        }
        
        bed.onPlant.AddListener(SetNewBed);
        agent.SetDestination(bed.transform.position);
    }

    private void BlockBed()
    {
        PlantInBed();
        
        removeBuryedPlantInteraction.onChannelCompleted.AddListener(Unburry);
        agent.onStrategyChanged+=Unburry;
        
        agent.StopAgent();
    }

    private void PlantInBed()
    {
        photonView.RPC(
            "RPC_BlockBedsStrategy_PlantInBed", 
            PhotonTargets.All,
            bed.photonView.viewID);
    }

    [PunRPC]
    [UsedImplicitly]
    private void RPC_BlockBedsStrategy_PlantInBed(int bedId)
    {
        bed = PhotonUtils.GetComponentByViewId<Planting>(bedId);
        inBed = true;
        removeBuryedPlantInteraction.IsBuryed = true;
        bed.Plant(gameObject);
    }
    
    private void Unburry()
    {
        removeBuryedPlantInteraction.onChannelCompleted.RemoveListener(Unburry);
        agent.onStrategyChanged-=Unburry;
        
        RemoveFromBed();
        GetComponent<Agent>().StartAgent();
    }
    
    private void RemoveFromBed()
    {
        photonView.RPC(
            "RPC_BlockBedsStrategy_RemoveFromBed", 
            PhotonTargets.All,
            bed.photonView.viewID);
    }
    
    [PunRPC]
    [UsedImplicitly]
    private void RPC_BlockBedsStrategy_RemoveFromBed(int bedId)
    {
        bed = PhotonUtils.GetComponentByViewId<Planting>(bedId);
        inBed = false;
        removeBuryedPlantInteraction.IsBuryed = false;
        bed.ClearBed();
    }
    
    public override float GetMinDistanceToDestination()
    {
        return MinDistanceToDestination;
    }
}
