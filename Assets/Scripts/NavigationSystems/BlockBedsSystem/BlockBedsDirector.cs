﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BlockBedsDirector : Photon.MonoBehaviour
{
    public static BlockBedsDirector instance;
    public GameObject spawnPoint;
    
    private List<Planting> beds;

    private void Awake()
    {
        instance = this;
        beds = FindObjectsOfType<Planting>().ToList();
    }
    
    public Planting GetAttractiveBed(Planting oldBed = null)
    {
        List<Planting> viablePlantingLocations = beds.FindAll(p => !p.IsSaplingPlanted() && p != oldBed);

        return viablePlantingLocations.Count == 0 ? null : viablePlantingLocations[Random.Range(0, viablePlantingLocations.Count)];
    }
}