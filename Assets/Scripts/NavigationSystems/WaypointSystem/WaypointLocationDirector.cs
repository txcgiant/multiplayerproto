﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class WaypointLocationDirector : MonoBehaviour
{
	public static WaypointLocationDirector instance;
	
	private readonly List<WaypointLocation> waypointLocations = new List<WaypointLocation>();

	private void Awake()
	{
		instance = this;
	}

	public void RegisterWaypointLocation(WaypointLocation suicideLocation)
	{
		waypointLocations.Add(suicideLocation);
	}

	public WaypointLocation GetWaypointLocation(PlantType plantType, WaypointLocation oldSuicideLocation = null)
	{
		WaypointLocation newSuicideLocation = FindAttractiveWaypointLocation(plantType, oldSuicideLocation);

		return newSuicideLocation != null ? newSuicideLocation : oldSuicideLocation;
	}

	private WaypointLocation FindAttractiveWaypointLocation(PlantType plantType, WaypointLocation oldSuicideLocation)
	{
		SortWaypointLocations();
		
		foreach (WaypointLocation waypointLocation in waypointLocations) {
			if (!waypointLocation.IsForPlantType(plantType)) {
				continue;
			}
			if (waypointLocation != oldSuicideLocation) {
				return waypointLocation;
			}
		}

		return null;
	}

	private void SortWaypointLocations()
	{
		waypointLocations
			.Sort((a,b) => a.GetPriority().CompareTo(b.GetPriority()));
	}
}