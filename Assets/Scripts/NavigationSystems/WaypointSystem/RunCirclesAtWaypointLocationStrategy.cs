﻿using System;
using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[RequireComponent(typeof(RunCirclesAtWaypointLocationStrategy))]
[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(Agent))]
public class RunCirclesAtWaypointLocationStrategy : MovementStrategyMonoBehaviour
{
    public float timeAtWaypointLocation = 30f;
    
    [UsedImplicitly]
    public UnityEvent onLocationChanged;
    
    private const float MinDistanceToDestination = 0.5f;
    private PlantType plantType;
    
    private Agent agent;
    private WaypointLocation waypointLocation;
    private int waypointIndex;
    private bool reverseWaypointOrder;
    private bool atWaypointLocation;

    private void Awake()
    {
        plantType = GetComponent<Plant>().plantType;
    }

    public override void OnDestinationReached(Agent agent)
    {
        if (!atWaypointLocation) {
            atWaypointLocation = true;
            CalculateWaypointOrder();
            StartCoroutine(SwitchWaypointLocationInSeconds(timeAtWaypointLocation));
        }

        waypointIndex = NextWaypointIndex(reverseWaypointOrder);
        agent.SetDestination(waypointLocation.GetWaypoint(waypointIndex));
    }

    public override void OnStart(Agent agent)
    {
        this.agent = agent;
        SetNewWaypointLocation();
    }

    [UsedImplicitly]
    public override void OnStop(Agent agent)
    {
        StopAllCoroutines();
        
        if (waypointLocation == null) {
            return;
        }

        waypointLocation.numberAgents--;
        waypointLocation = null;
    }

    private void SetNewWaypointLocation(WaypointLocation oldWaypointLocation = null)
    {
        waypointLocation = WaypointLocationDirector.instance.GetWaypointLocation(plantType, oldWaypointLocation);
        waypointLocation.numberAgents++;
        atWaypointLocation = false;
        waypointIndex = Random.Range(0, waypointLocation.GetNumberWaypoints());
        agent.SetDestination(waypointLocation.GetWaypoint(waypointIndex));
    }

    private int NextWaypointIndex(bool reverserOrder)
    {
        if (reverserOrder) {
            return (waypointIndex == 0) ? waypointLocation.GetNumberWaypoints() - 1 : waypointIndex - 1;
        }

        return (waypointIndex >= waypointLocation.GetNumberWaypoints() - 1) ? 0 : waypointIndex + 1;
    }

    private void CalculateWaypointOrder()
    {
        reverseWaypointOrder = AngleToWaypoint(NextWaypointIndex(false)) < AngleToWaypoint(NextWaypointIndex(true));
    }

    private float AngleToWaypoint(int waypointIndex)
    {
        return Math.Abs(Vector3.SignedAngle(transform.position - waypointLocation.GetWaypoint(waypointIndex), transform.forward,
            Vector3.up));
    }
    
    public override float GetMinDistanceToDestination()
    {
        return MinDistanceToDestination;
    }
    
    private IEnumerator SwitchWaypointLocationInSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        SetNewWaypointLocation(waypointLocation);
        onLocationChanged?.Invoke();
    }
}