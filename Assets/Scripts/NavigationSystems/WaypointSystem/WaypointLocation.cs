﻿using System.Collections.Generic;
using UnityEngine;

public class WaypointLocation : LocationOfInterest
{
    private readonly List<GameObject> waypoints = new List<GameObject>();

    private void Awake()
    {
        foreach (Transform child in transform) {
            waypoints.Add(child.gameObject);
        }

        WaypointLocationDirector.instance.RegisterWaypointLocation(this);
    }

    public Vector3 GetWaypoint(int id)
    {
        return waypoints[id].transform.position;
    }

    public int GetNumberWaypoints()
    {
        return waypoints.Count;
    }
}