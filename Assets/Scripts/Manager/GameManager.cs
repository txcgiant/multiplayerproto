﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

public class GameManager : Photon.PunBehaviour
{
    public static GameManager instance;

    public GameSettings settings;
    
    public GameObject[] spawnPoints;
    public GameObject plantContainer;
    public RecipeBook recipeBook;
    public PlantMatcher plantMatcher;
    
    [HideInInspector]
    public Player localPlayer;

    public UnityEvent onGameOver;
    public FloatUnityEvent onTimerUpdate;
    
    private InputManager inputManager;

    private float currentTime;
    private bool isRunning;
    	
    private void Awake()
    {
        if (instance != null) {
            Destroy(gameObject);
        } else {
            instance = this;
            StatusEffects.Init();
        }
    }
    
    private void OnEnable()
    {
        PhotonNetwork.OnEventCall += OnEvent;
    }
    
    private void OnDisable()
    {
        PhotonNetwork.OnEventCall -= OnEvent;
    }

    private void Start()
    {
        inputManager = InputManager.instance;
        currentTime = 0f;
        
        if (PhotonNetwork.connectedAndReady) {
            SpawnPlayer();
        }

        if (PhotonNetwork.isMasterClient) {
            PhotonNetwork.RaiseEvent(
                RaiseEvents.StartTimer,
                currentTime,
                true,
                new RaiseEventOptions { Receivers = ReceiverGroup.All }
            );
        }
    }

    public void SpawnPlayer()
    {
        GameObject spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
		
        Vector3 position = Vector3.zero;
		
        if (spawnPoint != null) {
            position = spawnPoint.transform.position;
        }

        Player player = PhotonNetwork.Instantiate(
            "Player",
            position,
            Quaternion.Euler(new Vector3(0, 180, 0)), 0).GetComponent<Player>();

        if (player.photonView.isMine) {
            localPlayer = player;
        }
    }
    
    public void Update()
    {
        if (inputManager.ExitKeyDown) {
            LeaveRoom();
        }

        if (!isRunning) {
            return;
        }
        
        if (currentTime >= settings.playDuration) {
            StartCoroutine(GameOver());
        }
    }

    public IEnumerator GameOver()
    {
        isRunning = false;
        onGameOver?.Invoke();
        
        yield return new WaitForSeconds(settings.gameOverDelay);
        
        PhotonNetwork.room
            .SetCustomProperties(new Hashtable {{RoomCustomProperties.Time, currentTime}});
        
        SceneManager.LoadScene(settings.gameOverSceneName);
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(settings.launchSceneName);
    }

    [UsedImplicitly]
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
    }
    
    private IEnumerator IncreaseTimer()
    {
        while (isRunning) {
            yield return new WaitForSeconds(1);
            IncreaseTime();
        }
    }

    private void IncreaseTime()
    {
        currentTime++;
        onTimerUpdate?.Invoke(currentTime);
    }

    public void IncreaseLevelTime(float time)
    {
        currentTime -= time;
    }

    private void OnEvent(byte eventcode, object content, int senderid)
    {
        if (eventcode == RaiseEvents.StartTimer) {
            StartTimer();
        }
    }

    public void StartTimer()
    {
        onTimerUpdate?.Invoke(currentTime);
        isRunning = true;
        StartCoroutine(IncreaseTimer());
    }
}