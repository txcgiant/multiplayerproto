﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    
    public void Awake()
    {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(this);
        }
    }

    [SerializeField]
    private KeyCode exitKey = KeyCode.Escape;
    
    public Vector2 LeftStick { get; private set; }
    public Vector2 Mouse { get; private set; }
    
    public float HorizontalRightStick { get; private set; }
    public float VerticalRightStick { get; private set; }
    
    public bool DashKeyPressed { get; private set; }
    public bool RecipeKeyDown { get; private set; }
    public bool ExitKeyDown { get; private set; }

    public Action onUseKeyDown;
    public Action onUseKeyUp;

    public Action onShootKeyDown;
    public Action onShootKeyUp;

    public Action onDashKeyDown;
    
    public void Update()
    {
        LeftStick = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Mouse = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        
        if (Input.GetButtonDown("Dash Keyboard") || Input.GetButtonDown("Dash Controller")) {
            DashKeyPressed = true;
        } else if (Input.GetButtonUp("Dash Keyboard") || Input.GetButtonUp("Dash Controller")) {
            DashKeyPressed = false;
        }
        
        RecipeKeyDown = Input.GetButtonDown("Recipes");
        ExitKeyDown = Input.GetKeyDown(exitKey);
        HorizontalRightStick = Input.GetAxis("Right Thumbstick Horizontal");
        VerticalRightStick = Input.GetAxis("Right Thumbstick Vertical");

        if (DashKeyPressed) {
            onDashKeyDown?.Invoke();
        }
        
        if (Input.GetButtonDown("Interact")) {
            onUseKeyDown?.Invoke();
        } else if (Input.GetButtonUp("Interact")) {
            onUseKeyUp?.Invoke();
        }
        
        if (Input.GetButtonDown("Shoot")) {
            onShootKeyDown?.Invoke();
        } else if (Input.GetButtonUp("Shoot")) {
            onShootKeyUp?.Invoke();
        }
    }
    
    public bool IsLeftStickMoving()
    {
        return LeftStick.magnitude > 0.01f;
    }
    
    public bool IsRightStickMoving()
    {
        return Input.GetAxis("Right Thumbstick Horizontal") > 0 || Input.GetAxis("Right Thumbstick Horizontal") < 0 || 
               Input.GetAxis("Right Thumbstick Vertical") > 0 || Input.GetAxis("Right Thumbstick Vertical") < 0;  
    }

    public bool IsMouseMoving()
    {
        return Mouse.magnitude > 0.01;
    }
}