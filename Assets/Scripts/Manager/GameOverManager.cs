﻿using System;
using JetBrains.Annotations;
using TMPro;
using UnityEngine.SceneManagement;

public class GameOverManager : Photon.PunBehaviour
{
    public Highscore highscore;
    
    public TextMeshProUGUI score;
    public TMP_InputField teamname;

    private float teamTime;
    
    public void Start()
    {
        if (!PhotonNetwork.connectedAndReady) {
            return;
        }
        
        if (PhotonNetwork.isMasterClient) {
            teamname.gameObject.SetActive(true);
            teamname.interactable = true;
        }
        
        teamTime = (float)PhotonNetwork.room.CustomProperties[RoomCustomProperties.Time];

        ShowTime();
    }

    private void ShowTime()
    {
        TimeSpan t = TimeSpan.FromSeconds( (int)teamTime);
        score.text = $"{t.Minutes:D2}:{t.Seconds:D2}"; 
    }
    
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Launcher");
    }

    [UsedImplicitly]
    public void LeaveRoom()
    {
        if (!PhotonNetwork.connected) {
            return;
        }

        if (PhotonNetwork.isMasterClient) {
            SaveHighscore();
        }
        
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
    }

    private void SaveHighscore()
    {
        if (teamname.text.Equals(string.Empty)) {
            return;
        }
        
        highscore.Add(teamname.text, (int)teamTime);
    }
}