﻿using JetBrains.Annotations;
using UnityEngine;

public class TransformNetworkSync : Photon.MonoBehaviour, IPunObservable 
{
    private Vector3 networkPosition;
    private Quaternion networkRotation;

    private Vector3 velocity;

    private void OnEnable()
    {
        networkPosition = transform.position;
        networkRotation = transform.rotation;
    }

    private void FixedUpdate()
    {
        if (!photonView.isMine) {
            LerpTransform();
        }
    }

    private void LerpTransform()
    {
        transform.position = Vector3.Lerp(transform.position, networkPosition, Time.deltaTime * 10f);
        transform.rotation = Quaternion.Lerp(transform.rotation, networkRotation, Time.deltaTime * 10f);
    }

    [UsedImplicitly]
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting) {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        } else {
            networkPosition = (Vector3) stream.ReceiveNext();
            networkRotation = (Quaternion) stream.ReceiveNext();
			
            Vector3 oldPosition = transform.position;
            float lag = Mathf.Abs((float) (PhotonNetwork.time - info.timestamp));
            velocity = networkPosition - oldPosition;
            networkPosition += (velocity * lag);
        }
    }
}
