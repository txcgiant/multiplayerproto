﻿public abstract class RaiseEvents
{
    public const byte ReadyCheck = 0;
    public const byte GameStarted = 1;
    public const byte StartTimer = 2;
}