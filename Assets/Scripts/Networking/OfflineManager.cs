﻿using JetBrains.Annotations;

public class OfflineManager : Photon.PunBehaviour
{
    public bool dontSpawnPlayer;
    
    private void Awake()
    {
        PhotonNetwork.autoJoinLobby = false;
    }

    private void Start()
    {
        if (!PhotonNetwork.connectedAndReady) {
            StartGame();
        }
    }

    [UsedImplicitly]
    public void StartGame()
    {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.JoinOrCreateRoom("offline", null, null);
    }
    
    [UsedImplicitly]
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.offlineMode && !dontSpawnPlayer) {
            GameManager.instance.SpawnPlayer();
            GameManager.instance.StartTimer();
        }
    }
}