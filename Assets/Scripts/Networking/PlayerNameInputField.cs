﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using MonoBehaviour = Photon.MonoBehaviour;

[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour
{
    private const string PlayerNamePrefKey = "PlayerName";
    
    private void Start()
    {
        InputField inputField = GetComponent<InputField>();
        
        if (inputField == null) {
            return;
        }

        string defaultName = PlayerPrefs.GetString(PlayerNamePrefKey) ?? string.Empty;

        inputField.text = defaultName;
        PhotonNetwork.playerName = defaultName;
    }

    [UsedImplicitly]
    public void SetPlayerName(string value)
    {
        PhotonNetwork.playerName = value + string.Empty;
        PlayerPrefs.SetString(PlayerNamePrefKey, value);
    }
}