﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
 
public class NetworkLauncher : Photon.PunBehaviour
{
 private const string GameVersion = "Monkey";
 
 [Tooltip("The maximum number of players per room.")]
 [Range(1, 4)]
 public byte maxPlayersPerRoom = 4;
 public PhotonLogLevel loglevel = PhotonLogLevel.Informational;

 public InputField roomNameInputField;

 private void Awake()
 {
     PhotonNetwork.autoJoinLobby = false;
     PhotonNetwork.automaticallySyncScene = true;
     PhotonNetwork.logLevel = loglevel;
 }
 
 [UsedImplicitly]
 public void Connect()
 {
     if (PhotonNetwork.connected) {
         Debug.Log("Already connected to server");

     } else {
         PhotonNetwork.ConnectUsingSettings(GameVersion);
     }
 }

 [UsedImplicitly]
 public override void OnConnectedToMaster()
 {
     if (!PhotonNetwork.offlineMode) {
         PhotonNetwork.JoinLobby();
     }
 }

 public override void OnJoinedLobby()
 {
     string roomName = roomNameInputField.text;

     if (roomName.Equals(string.Empty)) {
         roomName = Guid.NewGuid().ToString();
     }
     
     PhotonNetwork.JoinOrCreateRoom(
         roomName,
         new RoomOptions() { MaxPlayers = maxPlayersPerRoom},
         null);
 }
 
 public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
 {
     PhotonNetwork.Disconnect();
 }
 
 [UsedImplicitly]
 public void ExitGame()
 {
     if (PhotonNetwork.connected) {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
     }
     
     Application.Quit();
 }
}