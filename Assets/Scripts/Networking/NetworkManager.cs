﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class NetworkManager : Photon.PunBehaviour
{
	[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
	public byte maxPlayersPerRoom = 4;
	public PhotonLogLevel loglevel = PhotonLogLevel.Informational;

	private const string GameVersion = "Monkey";

	private void Awake()
	{
		PhotonNetwork.autoJoinLobby = false;
		PhotonNetwork.automaticallySyncScene = true;
		PhotonNetwork.logLevel = loglevel;
	}

	[UsedImplicitly]
	public void Offline()
	{
		PhotonNetwork.offlineMode = true;
		PhotonNetwork.CreateRoom("offline");
	}

	// Called from Login-Button
	[UsedImplicitly]
	public void Connect()
	{
		if (PhotonNetwork.connected) {
			PhotonNetwork.JoinRandomRoom();
			Debug.Log("Already connected to server");

		} else {
			PhotonNetwork.ConnectUsingSettings(GameVersion);
		}
	}

	[UsedImplicitly]
	public override void OnConnectedToMaster()
	{
		Debug.Log("Connected to Master");

		if (!PhotonNetwork.offlineMode) {
			PhotonNetwork.JoinLobby();
		}

		//menu.SetActive(false);
	}

	public override void OnDisconnectedFromPhoton()
	{
		Debug.Log("OnDisconnectedFromPhoton");
	}

	[UsedImplicitly]
	public override void OnJoinedLobby()
	{
		Debug.Log("Connected to Lobby");
		PhotonNetwork.JoinRandomRoom();
	}

	[UsedImplicitly]
	public override void OnJoinedRoom()
	{
		Spawn();
	}

	[UsedImplicitly]
	private void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(
			null,
			new RoomOptions() { MaxPlayers = maxPlayersPerRoom },
			null);
	}

	private void Spawn()
	{
		GameObject[] spawnPoints = GameManager.instance.spawnPoints;
		GameObject spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
		
		Vector3 position = Vector3.zero;
		
		if (spawnPoint != null) {
			position = spawnPoint.transform.position;
		}

		PhotonNetwork.Instantiate("Player", position, Quaternion.identity, 0);
	}

	[UsedImplicitly]
	public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		PhotonNetwork.RemoveRPCs(player);
		PhotonNetwork.DestroyPlayerObjects(player);
	}
}
