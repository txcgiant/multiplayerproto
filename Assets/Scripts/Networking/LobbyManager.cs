﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class LobbyManager : Photon.PunBehaviour
{
    public GameSettings settings;
    
    public GameObject launcherMenu;
    public GameObject roomLobby;
    
    public TextMeshProUGUI roomNameLabel;
    public Button startButton;

    public Slider loading;

    public PlayerLobbyDisplay[] playerLobbyDisplays;
    private List<PhotonPlayer> playersOrderedById;

    // player id, player panel
    private Dictionary<int, PlayerLobbyDisplay> displays;

    private List<int> readyPlayer;
    
    private void OnEnable()
    {
        PhotonNetwork.OnEventCall += OnEvent;
    }
    
    private void OnDisable()
    {
        PhotonNetwork.OnEventCall -= OnEvent;
    }
    
    private void OnEvent(byte eventcode, object content, int senderid)
    {
        switch (eventcode) {
            case RaiseEvents.ReadyCheck:
                bool isOn = (bool) content;

                if (isOn) {
                    readyPlayer.Add(senderid);
                }
            
                SetRoomCustomProperties();
            
                displays[senderid].playerReady.isOn = isOn;
                UpdateStartButton();
                break;
            case RaiseEvents.GameStarted:
                roomLobby.SetActive(false);
                loading.gameObject.SetActive(true);

                loading.DOValue(1, 1);
                break;
        }
    }

    private void Start()
    {
        playersOrderedById = new List<PhotonPlayer>();
        displays = new Dictionary<int, PlayerLobbyDisplay>();
        
        launcherMenu.SetActive(true);
        roomLobby.SetActive(false);
        readyPlayer = new List<int>();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        UpdatePlayerDisplays();
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        readyPlayer.Remove(otherPlayer.ID);
        UpdatePlayerDisplays();
    }

    private void UpdatePlayerDisplays()
    {
        SortPlayerListByIdAndMasterClient();
        ResetDisplays();
        
        for (int i = 0; i < playersOrderedById.Count; i++) {
            UpdatePlayerDisplay(i, playersOrderedById[i], playerLobbyDisplays[i]);
        }

        UpdateStartButton();
    }

    private void SortPlayerListByIdAndMasterClient()
    {
        playersOrderedById = PhotonNetwork.playerList.ToList();
        playersOrderedById.Sort((player1, player2) => player1.ID.CompareTo(player2.ID));
        
        playersOrderedById = playersOrderedById
            .OrderBy(player => !player.IsMasterClient)
            .ToList();
    }

    private void UpdatePlayerDisplay(int index, PhotonPlayer player, PlayerLobbyDisplay display)
    {
        display.gameObject.SetActive(true);
        display.background.color = settings.playerColorMaterials.materials[index].color;
        display.playerName.text = /*player.ID + " | " +*/ player.NickName;
        display.playerReady.interactable = player.IsLocal;
        display.playerReady.isOn = IsPlayerReady(player.ID);

        display.playerReady.onClick += OnToggleClicked;
        player.SetCustomProperties(new Hashtable {{PlayerCustomProperties.ColorIndex, index}});
        
        //PhotonNetwork.room.SetCustomProperties(new Hashtable {{RoomCustomProperties.PlayerColors, index}});
            
        displays.Add(player.ID, display);
    }

    private void OnToggleClicked(bool isOn)
    {
        RaiseEventOptions option = new RaiseEventOptions {CachingOption = EventCaching.AddToRoomCache, Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(RaiseEvents.ReadyCheck, isOn, true, option);
    }
    
    private void UpdateStartButton()
    {
        startButton.interactable = ReadyToStart();
    }

    private bool IsPlayerReady(int playerId)
    {
        int[] ready = (int[])PhotonNetwork.room.CustomProperties[RoomCustomProperties.ReadyPlayers];
        return ready.Contains(playerId);
    }

    private void ResetDisplays()
    {
        foreach (PlayerLobbyDisplay display in displays.Values) {
            display.playerReady.onClick -= OnToggleClicked;
            display.Reset();
        }
        
        displays.Clear();
    }

    public override void OnJoinedRoom()
    {
        launcherMenu.SetActive(false);
        roomLobby.SetActive(true);

        SetRoomCustomProperties();
        roomNameLabel.text = PhotonNetwork.room.Name;

        UpdatePlayerDisplays();
    }

    private void SetRoomCustomProperties()
    {
        PhotonNetwork.room
            .SetCustomProperties(new Hashtable {{RoomCustomProperties.ReadyPlayers, readyPlayer.ToArray()}});
    }

    public override void OnLeftRoom()
    {
        launcherMenu.SetActive(true);
        roomLobby.SetActive(false);
    }
    
    [UsedImplicitly]
    public void StartGame()
    {
        if (!AllPlayersAreReady()) {
            return;
        }

        ResetDisplays();
        roomLobby.SetActive(false);
        loading.gameObject.SetActive(true);
        PhotonNetwork.room.IsOpen = false;
        PhotonNetwork.room.IsVisible = false;
        
        StartCoroutine(LoadSceneAsync());
    }

    private IEnumerator LoadSceneAsync()
    {
        RaiseEventOptions option = new RaiseEventOptions {CachingOption = EventCaching.DoNotCache, Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(RaiseEvents.GameStarted, null, true, option);
        
        AsyncOperation operation = PhotonNetwork.LoadLevelAsync(settings.gameSceneName);
        
        while (!operation.isDone) {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            loading.value = progress;
            yield return null;
        }
    }

    [UsedImplicitly]
    public void ExitGame()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
    }

    private bool AllPlayersAreReady()
    {
        return displays.Values.All(display => display.playerReady.isOn);
    }

    private bool ReadyToStart()
    {
        return PhotonNetwork.isMasterClient && AllPlayersAreReady();
    }
}
